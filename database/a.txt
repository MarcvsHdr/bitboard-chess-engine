 [ECO "A00"]
[Opening "Polish (Sokolsky) opening"]

1. b4

[ECO "A00"]
[Opening "Polish"]
[Variation "Tuebingen variation"]

1. b4 Nh6

[ECO "A00"]
[Opening "Polish"]
[Variation "Outflank variation"]

1. b4 c6

[ECO "A00"]
[Opening "Benko's Opening"]

1. g3

[ECO "A00"]
[Opening "Lasker simul special"]

1. g3 h5

[ECO "A00"]
[Opening "Benko's Opening"]
[Variation "reversed Alekhine"]

1. g3 e5 2. Nf3

[ECO "A00"]
[Opening "Grob's attack"]

1. g4

[ECO "A00"]
[Opening "Grob"]
[Variation "Spike attack"]

1. g4 d5 2. Bg2 c6 3. g5

[ECO "A00"]
[Opening "Grob"]
[Variation "Fritz gambit"]

1. g4 d5 2. Bg2 Bxg4 3. c4

[ECO "A00"]
[Opening "Grob"]
[Variation "Romford counter-gambit"]

1. g4 d5 2. Bg2 Bxg4 3. c4 d4

[ECO "A00"]
[Opening "Clemenz (Mead's, Basman's or de Klerk's) Opening"]

1. h3

[ECO "A00"]
[Opening "Global Opening"]

1. h3 e5 2. a3

[ECO "A00"]
[Opening "Amar (Paris) Opening"]

1. Nh3

[ECO "A00"]
[Opening "Amar gambit"]

1. Nh3 d5 2. g3 e5 3. f4 Bxh3 4. Bxh3 exf4

[ECO "A00"]
[Opening "Dunst (Sleipner, Heinrichsen) Opening"]

1. Nc3

[ECO "A00"]
[Opening "Dunst (Sleipner,Heinrichsen) Opening"]

1. Nc3 e5

[ECO "A00"]
[Opening "Battambang Opening"]

1. Nc3 e5 2. a3

[ECO "A00"]
[Opening "Novosibirsk Opening"]

1. Nc3 c5 2. d4 cxd4 3. Qxd4 Nc6 4. Qh4

[ECO "A00"]
[Opening "Anderssen's Opening"]

1. a3

[ECO "A00"]
[Opening "Ware (Meadow Hay) Opening"]

1. a4

[ECO "A00"]
[Opening "Crab Opening"]

1. a4 e5 2. h4

[ECO "A00"]
[Opening "Saragossa Opening"]

1. c3

[ECO "A00"]
[Opening "Mieses Opening"]

1. d3

[ECO "A00"]
[Opening "Mieses Opening"]

1. d3 e5

[ECO "A00"]
[Opening "Valencia Opening"]

1. d3 e5 2. Nd2

[ECO "A00"]
[Opening "Venezolana Opening"]

1. d3 c5 2. Nc3 Nc6 3. g3

[ECO "A00"]
[Opening "Van't Kruijs Opening"]

1. e3

[ECO "A00"]
[Opening "Amsterdam attack"]

1. e3 e5 2. c4 d6 3. Nc3 Nc6 4. b3 Nf6

[ECO "A00"]
[Opening "Gedult's Opening"]

1. f3

[ECO "A00"]
[Opening "Hammerschlag (Fried fox/Pork chop Opening)"]

1. f3 e5 2. Kf2

[ECO "A00"]
[Opening "Anti-Borg (Desprez) Opening"]

1. h4

[ECO "A00"]
[Opening "Durkin's attack"]

1. Na3

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]

1. b3

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]
[Variation "Modern variation"]

1. b3 e5

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]
[Variation "Indian variation"]

1. b3 Nf6

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]
[Variation "Classical variation"]

1. b3 d5

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]
[Variation "English variation"]

1. b3 c5

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]
[Variation "Dutch variation"]

1. b3 f5

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]
[Variation "Polish variation"]

1. b3 b5

[ECO "A01"]
[Opening "Nimzovich-Larsen attack"]
[Variation "Symmetrical variation"]

1. b3 b6

[ECO "A02"]
[Opening "Bird's Opening"]

1. f4

[ECO "A02"]
[Opening "Bird"]
[Variation "From gambit"]

1. f4 e5

[ECO "A02"]
[Opening "Bird"]
[Variation "From gambit, Lasker variation"]

1. f4 e5 2. fxe5 d6 3. exd6 Bxd6 4. Nf3 g5

[ECO "A02"]
[Opening "Bird"]
[Variation "From gambit, Lipke variation"]

1. f4 e5 2. fxe5 d6 3. exd6 Bxd6 4. Nf3 Nh6 5. d4

[ECO "A02"]
[Opening "Bird's Opening, Swiss gambit"]

1. f4 f5 2. e4 fxe4 3. Nc3 Nf6 4. g4

[ECO "A02"]
[Opening "Bird"]
[Variation "Hobbs gambit"]

1. f4 g5

[ECO "A03"]
[Opening "Bird's Opening"]

1. f4 d5

[ECO "A03"]
[Opening "Mujannah Opening"]

1. f4 d5 2. c4

[ECO "A03"]
[Opening "Bird's Opening"]
[Variation "Williams gambit"]

1. f4 d5 2. e4

[ECO "A03"]
[Opening "Bird's Opening"]
[Variation "Lasker variation"]

1. f4 d5 2. Nf3 Nf6 3. e3 c5

[ECO "A04"]
[Opening "Reti Opening"]

1. Nf3

[ECO "A04"]
[Opening "Reti v Dutch"]

1. Nf3 f5

[ECO "A04"]
[Opening "Reti"]
[Variation "Pirc-Lisitsin gambit"]

1. Nf3 f5 2. e4

[ECO "A04"]
[Opening "Reti"]
[Variation "Lisitsin gambit deferred"]

1. Nf3 f5 2. d3 Nf6 3. e4

[ECO "A04"]
[Opening "Reti Opening"]

1. Nf3 d6

[ECO "A04"]
[Opening "Reti"]
[Variation "Wade defence"]

1. Nf3 d6 2. e4 Bg4

[ECO "A04"]
[Opening "Reti"]
[Variation "Herrstroem gambit"]

1. Nf3 g5

[ECO "A05"]
[Opening "Reti Opening"]

1. Nf3 Nf6

[ECO "A05"]
[Opening "Reti"]
[Variation "King's Indian attack, Spassky's variation"]

1. Nf3 Nf6 2. g3 b5

[ECO "A05"]
[Opening "Reti"]
[Variation "King's Indian attack"]

1. Nf3 Nf6 2. g3 g6

[ECO "A05"]
[Opening "Reti"]
[Variation "King's Indian attack, Reti-Smyslov variation"]

1. Nf3 Nf6 2. g3 g6 3. b4

[ECO "A06"]
[Opening "Reti Opening"]

1. Nf3 d5

[ECO "A06"]
[Opening "Reti"]
[Variation "Old Indian attack"]

1. Nf3 d5 2. d3

[ECO "A06"]
[Opening "Santasiere's folly"]

1. Nf3 d5 2. b4

[ECO "A06"]
[Opening "Tennison (Lemberg, Zukertort) gambit"]

1. Nf3 d5 2. e4

[ECO "A06"]
[Opening "Reti"]
[Variation "Nimzovich-Larsen attack"]

1. Nf3 d5 2. b3

[ECO "A07"]
[Opening "Reti"]
[Variation "King's Indian attack (Barcza system)"]

1. Nf3 d5 2. g3

[ECO "A07"]
[Opening "Reti"]
[Variation "King's Indian attack, Yugoslav variation"]

1. Nf3 d5 2. g3 Nf6 3. Bg2 c6 4. O-O Bg4

[ECO "A07"]
[Opening "Reti"]
[Variation "King's Indian attack, Keres variation"]

1. Nf3 d5 2. g3 Bg4 3. Bg2 Nd7

[ECO "A07"]
[Opening "Reti"]
[Variation "King's Indian attack"]

1. Nf3 d5 2. g3 g6

[ECO "A07"]
[Opening "Reti"]
[Variation "King's Indian attack, Pachman system"]

1. Nf3 d5 2. g3 g6 3. Bg2 Bg7 4. O-O e5 5. d3 Ne7

[ECO "A07"]
[Opening "Reti"]
[Variation "King's Indian attack (with ...c5)"]

1. Nf3 d5 2. g3 c5

[ECO "A08"]
[Opening "Reti"]
[Variation "King's Indian attack"]

1. Nf3 d5 2. g3 c5 3. Bg2

[ECO "A08"]
[Opening "Reti"]
[Variation "King's Indian attack, French variation"]

1. Nf3 d5 2. g3 c5 3. Bg2 Nc6 4. O-O e6 5. d3 Nf6 6. Nbd2 Be7 7. e4 O-O 8. Re1

[ECO "A09"]
[Opening "Reti Opening"]

1. Nf3 d5 2. c4

[ECO "A09"]
[Opening "Reti"]
[Variation "Advance variation"]

1. Nf3 d5 2. c4 d4

[ECO "A09"]
[Opening "Reti accepted"]

1. Nf3 d5 2. c4 dxc4

[ECO "A09"]
[Opening "Reti accepted"]
[Variation "Keres variation"]

1. Nf3 d5 2. c4 dxc4 3. e3 Be6

[ECO "A10"]
[Opening "English Opening"]

1. c4

[ECO "A10"]
[Opening "English Opening"]

1. c4 g6

[ECO "A10"]
[Opening "English"]
[Variation "Adorjan defence"]

1. c4 g6 2. e4 e5

[ECO "A10"]
[Opening "English"]
[Variation "Jaenisch gambit"]

1. c4 b5

[ECO "A10"]
[Opening "English"]
[Variation "Anglo-Dutch defense"]

1. c4 f5

[ECO "A11"]
[Opening "English"]
[Variation "Caro-Kann defensive system"]

1. c4 c6

[ECO "A12"]
[Opening "English"]
[Variation "Caro-Kann defensive system"]

1. c4 c6 2. Nf3 d5 3. b3

[ECO "A12"]
[Opening "English"]
[Variation "Torre defensive system"]

1. c4 c6 2. Nf3 d5 3. b3 Nf6 4. g3 Bg4

[ECO "A12"]
[Opening "English"]
[Variation "London defensive system"]

1. c4 c6 2. Nf3 d5 3. b3 Nf6 4. g3 Bf5

[ECO "A12"]
[Opening "English"]
[Variation "Caro-Kann defensive system"]

1. c4 c6 2. Nf3 d5 3. b3 Nf6 4. Bb2

[ECO "A12"]
[Opening "English"]
[Variation "Bled variation"]

1. c4 c6 2. Nf3 d5 3. b3 Nf6 4. Bb2 g6

[ECO "A12"]
[Opening "English"]
[Variation "New York (London) defensive system"]

1. c4 c6 2. Nf3 d5 3. b3 Nf6 4. Bb2 Bf5

[ECO "A12"]
[Opening "English"]
[Variation "Capablanca's variation"]

1. c4 c6 2. Nf3 d5 3. b3 Nf6 4. Bb2 Bg4

[ECO "A12"]
[Opening "English"]
[Variation "Caro-Kann defensive system, Bogolyubov variation"]

1. c4 c6 2. Nf3 d5 3. b3 Bg4

[ECO "A13"]
[Opening "English Opening"]

1. c4 e6

[ECO "A13"]
[Opening "English"]
[Variation "Romanishin gambit"]

1. c4 e6 2. Nf3 Nf6 3. g3 a6 4. Bg2 b5

[ECO "A13"]
[Opening "English Opening"]
[Variation "Agincourt variation"]

1. c4 e6 2. Nf3 d5

[ECO "A13"]
[Opening "English"]
[Variation "Wimpey system"]

1. c4 e6 2. Nf3 d5 3. b3 Nf6 4. Bb2 c5 5. e3

[ECO "A13"]
[Opening "English Opening"]
[Variation "Agincourt variation"]

1. c4 e6 2. Nf3 d5 3. g3

[ECO "A13"]
[Opening "English"]
[Variation "Kurajica defence"]

1. c4 e6 2. Nf3 d5 3. g3 c6

[ECO "A13"]
[Opening "English"]
[Variation "Neo-Catalan"]

1. c4 e6 2. Nf3 d5 3. g3 Nf6

[ECO "A13"]
[Opening "English"]
[Variation "Neo-Catalan accepted"]

1. c4 e6 2. Nf3 d5 3. g3 Nf6 4. Bg2 dxc4

[ECO "A14"]
[Opening "English"]
[Variation "Neo-Catalan declined"]

1. c4 e6 2. Nf3 d5 3. g3 Nf6 4. Bg2 Be7 5. O-O

[ECO "A14"]
[Opening "English"]
[Variation "Symmetrical, Keres defence"]

1. c4 e6 2. Nf3 d5 3. g3 Nf6 4. Bg2 Be7 5. O-O c5 6. cxd5 Nxd5 7. Nc3 Nc6

[ECO "A15"]
[Opening "English, 1...Nf6 (Anglo-Indian defense)"]

1. c4 Nf6

[ECO "A15"]
[Opening "English orang-utan"]

1. c4 Nf6 2. b4

[ECO "A15"]
[Opening "English Opening"]

1. c4 Nf6 2. Nf3

[ECO "A16"]
[Opening "English Opening"]

1. c4 Nf6 2. Nc3

[ECO "A16"]
[Opening "English"]
[Variation "Anglo-Gruenfeld defense"]

1. c4 Nf6 2. Nc3 d5

[ECO "A16"]
[Opening "English"]
[Variation "Anglo-Gruenfeld, Smyslov defense"]

1. c4 Nf6 2. Nc3 d5 3. cxd5 Nxd5 4. g3 g6 5. Bg2 Nxc3

[ECO "A16"]
[Opening "English"]
[Variation "Anglo-Gruenfeld, Czech defense"]

1. c4 Nf6 2. Nc3 d5 3. cxd5 Nxd5 4. g3 g6 5. Bg2 Nb6

[ECO "A16"]
[Opening "English"]
[Variation "Anglo-Gruenfeld defense"]

1. c4 Nf6 2. Nc3 d5 3. cxd5 Nxd5 4. Nf3

[ECO "A16"]
[Opening "English"]
[Variation "Anglo-Gruenfeld defense, Korchnoi variation"]

1. c4 Nf6 2. Nc3 d5 3. cxd5 Nxd5 4. Nf3 g6 5. g3 Bg7 6. Bg2 e5

[ECO "A17"]
[Opening "English Opening"]

1. c4 Nf6 2. Nc3 e6

[ECO "A17"]
[Opening "English"]
[Variation "Queens Indian formation"]

1. c4 Nf6 2. Nc3 e6 3. Nf3 b6

[ECO "A17"]
[Opening "English"]
[Variation "Queens Indian, Romanishin variation"]

1. c4 Nf6 2. Nc3 e6 3. Nf3 b6 4. e4 Bb7 5. Bd3

[ECO "A17"]
[Opening "English"]
[Variation "Nimzo-English Opening"]

1. c4 Nf6 2. Nc3 e6 3. Nf3 Bb4

[ECO "A18"]
[Opening "English"]
[Variation "Mikenas-Carls variation"]

1. c4 Nf6 2. Nc3 e6 3. e4

[ECO "A18"]
[Opening "English"]
[Variation "Mikenas-Carls, Flohr variation"]

1. c4 Nf6 2. Nc3 e6 3. e4 d5 4. e5

[ECO "A18"]
[Opening "English"]
[Variation "Mikenas-Carls, Kevitz variation"]

1. c4 Nf6 2. Nc3 e6 3. e4 Nc6

[ECO "A19"]
[Opening "English"]
[Variation "Mikenas-Carls, Sicilian variation"]

1. c4 Nf6 2. Nc3 e6 3. e4 c5

[ECO "A20"]
[Opening "English Opening"]

1. c4 e5

[ECO "A20"]
[Opening "English, Nimzovich variation"]

1. c4 e5 2. Nf3

[ECO "A20"]
[Opening "English, Nimzovich, Flohr variation"]

1. c4 e5 2. Nf3 e4

[ECO "A21"]
[Opening "English Opening"]

1. c4 e5 2. Nc3

[ECO "A21"]
[Opening "English, Troeger defence"]

1. c4 e5 2. Nc3 d6 3. g3 Be6 4. Bg2 Nc6

[ECO "A21"]
[Opening "English, Keres variation"]

1. c4 e5 2. Nc3 d6 3. g3 c6

[ECO "A21"]
[Opening "English Opening"]

1. c4 e5 2. Nc3 d6 3. Nf3

[ECO "A21"]
[Opening "English, Smyslov defence"]

1. c4 e5 2. Nc3 d6 3. Nf3 Bg4

[ECO "A21"]
[Opening "English, Kramnik-Shirov counterattack"]

1. c4 e5 2. Nc3 Bb4

[ECO "A22"]
[Opening "English Opening"]

1. c4 e5 2. Nc3 Nf6

[ECO "A22"]
[Opening "English"]
[Variation "Bellon gambit"]

1. c4 e5 2. Nc3 Nf6 3. Nf3 e4 4. Ng5 b5

[ECO "A22"]
[Opening "English"]
[Variation "Carls' Bremen system"]

1. c4 e5 2. Nc3 Nf6 3. g3

[ECO "A22"]
[Opening "English"]
[Variation "Bremen, reverse Dragon"]

1. c4 e5 2. Nc3 Nf6 3. g3 d5

[ECO "A22"]
[Opening "English"]
[Variation "Bremen, Smyslov system"]

1. c4 e5 2. Nc3 Nf6 3. g3 Bb4

[ECO "A23"]
[Opening "English"]
[Variation "Bremen system, Keres variation"]

1. c4 e5 2. Nc3 Nf6 3. g3 c6

[ECO "A24"]
[Opening "English"]
[Variation "Bremen system with ...g6"]

1. c4 e5 2. Nc3 Nf6 3. g3 g6

[ECO "A25"]
[Opening "English"]
[Variation "Sicilian Reversed"]

1. c4 e5 2. Nc3 Nc6

[ECO "A25"]
[Opening "English"]
[Variation "Closed system"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7

[ECO "A25"]
[Opening "English"]
[Variation "Closed, Taimanov variation"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. e3 d6 6. Nge2 Nh6

[ECO "A25"]
[Opening "English"]
[Variation "Closed, Hort variation"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. e3 d6 6. Nge2 Be6

[ECO "A25"]
[Opening "English"]
[Variation "Closed, 5.Rb1"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Rb1

[ECO "A25"]
[Opening "English"]
[Variation "Closed, 5.Rb1 Taimanov variation"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Rb1 Nh6

[ECO "A25"]
[Opening "English"]
[Variation "Closed system (without ...d6)"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3

[ECO "A26"]
[Opening "English"]
[Variation "Closed system"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6

[ECO "A26"]
[Opening "English"]
[Variation "Botvinnik system"]

1. c4 e5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6 6. e4

[ECO "A27"]
[Opening "English"]
[Variation "Three knights system"]

1. c4 e5 2. Nc3 Nc6 3. Nf3

[ECO "A28"]
[Opening "English"]
[Variation "Four knights system"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6

[ECO "A28"]
[Opening "English"]
[Variation "Nenarokov variation"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. d4 exd4 5. Nxd4 Bb4 6. Bg5 h6 7. Bh4 Bxc3+ 8. bxc3 Ne5

[ECO "A28"]
[Opening "English"]
[Variation "Bradley Beach variation"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. d4 e4

[ECO "A28"]
[Opening "English"]
[Variation "Four knights, Nimzovich variation"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. e4

[ECO "A28"]
[Opening "English"]
[Variation "Four knights, Marini variation"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. a3

[ECO "A28"]
[Opening "English"]
[Variation "Four knights, Capablanca variation"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. d3

[ECO "A28"]
[Opening "English"]
[Variation "Four knights, 4.e3"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. e3

[ECO "A28"]
[Opening "English"]
[Variation "Four knights, Stean variation"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. e3 Bb4 5. Qc2 O-O 6. Nd5 Re8 7. Qf5

[ECO "A28"]
[Opening "English"]
[Variation "Four knights, Romanishin variation"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. e3 Bb4 5. Qc2 Bxc3

[ECO "A29"]
[Opening "English"]
[Variation "Four knights, kingside Fianchetto"]

1. c4 e5 2. Nc3 Nc6 3. Nf3 Nf6 4. g3

[ECO "A30"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5

[ECO "A30"]
[Opening "English"]
[Variation "Symmetrical, hedgehog system"]

1. c4 c5 2. Nf3 Nf6 3. g3 b6 4. Bg2 Bb7 5. O-O e6 6. Nc3 Be7

[ECO "A30"]
[Opening "English"]
[Variation "Symmetrical, hedgehog, flexible formation"]

1. c4 c5 2. Nf3 Nf6 3. g3 b6 4. Bg2 Bb7 5. O-O e6 6. Nc3 Be7 7. d4 cxd4 8. Qxd4 d6 9. Rd1 a6 10. b3 Nbd7

[ECO "A31"]
[Opening "English"]
[Variation "Symmetrical, Benoni formation"]

1. c4 c5 2. Nf3 Nf6 3. d4

[ECO "A32"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nf3 Nf6 3. d4 cxd4 4. Nxd4 e6

[ECO "A33"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nf3 Nf6 3. d4 cxd4 4. Nxd4 e6 5. Nc3 Nc6

[ECO "A33"]
[Opening "English"]
[Variation "Symmetrical, Geller variation"]

1. c4 c5 2. Nf3 Nf6 3. d4 cxd4 4. Nxd4 e6 5. Nc3 Nc6 6. g3 Qb6

[ECO "A34"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nc3

[ECO "A34"]
[Opening "English"]
[Variation "Symmetrical, Three knights system"]

1. c4 c5 2. Nc3 Nf6 3. Nf3 d5 4. cxd5 Nxd5

[ECO "A34"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nc3 Nf6 3. g3

[ECO "A34"]
[Opening "English"]
[Variation "Symmetrical, Rubinstein system"]

1. c4 c5 2. Nc3 Nf6 3. g3 d5 4. cxd5 Nxd5 5. Bg2 Nc7

[ECO "A35"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nc3 Nc6

[ECO "A35"]
[Opening "English"]
[Variation "Symmetrical, Four knights system"]

1. c4 c5 2. Nc3 Nc6 3. Nf3 Nf6

[ECO "A36"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nc3 Nc6 3. g3

[ECO "A36"]
[Opening "English"]
[Variation "ultra-Symmetrical variation"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7

[ECO "A36"]
[Opening "English"]
[Variation "Symmetrical, Botvinnik system Reversed"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. e3 e5

[ECO "A36"]
[Opening "English"]
[Variation "Symmetrical, Botvinnik system"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. e4

[ECO "A37"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Nf3

[ECO "A37"]
[Opening "English"]
[Variation "Symmetrical, Botvinnik system Reversed"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Nf3 e5

[ECO "A38"]
[Opening "English"]
[Variation "Symmetrical variation"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Nf3 Nf6

[ECO "A38"]
[Opening "English"]
[Variation "Symmetrical, Main line with d3"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Nf3 Nf6 6. O-O O-O 7. d3

[ECO "A38"]
[Opening "English"]
[Variation "Symmetrical, Main line with b3"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Nf3 Nf6 6. O-O O-O 7. b3

[ECO "A39"]
[Opening "English"]
[Variation "Symmetrical, Main line with d4"]

1. c4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. Nf3 Nf6 6. O-O O-O 7. d4

[ECO "A40"]
[Opening "Queen's pawn"]

1. d4

[ECO "A40"]
[Opening "Queen's pawn"]
[Variation "Lundin (Kevitz-Mikenas) defence"]

1. d4 Nc6

[ECO "A40"]
[Opening "Queen's pawn"]
[Variation "Charlick (Englund) gambit"]

1. d4 e5

[ECO "A40"]
[Opening "Queen's pawn"]
[Variation "Englund gambit"]

1. d4 e5 2. dxe5 Nc6 3. Nf3 Qe7 4. Qd5 f6 5. exf6 Nxf6

[ECO "A40"]
[Opening "Queen's pawn"]
[Variation "English defence"]

1. d4 b6

[ECO "A40"]
[Opening "Polish defence"]

1. d4 b5

[ECO "A40"]
[Opening "Queen's pawn"]

1. d4 e6

[ECO "A40"]
[Opening "Queen's pawn"]
[Variation "Keres defence"]

1. d4 e6 2. c4 b6

[ECO "A40"]
[Opening "Queen's pawn"]
[Variation "Franco-Indian (Keres) defence"]

1. d4 e6 2. c4 Bb4+

[ECO "A40"]
[Opening "Modern defence"]

1. d4 g6

[ECO "A40"]
[Opening "Beefeater defence"]

1. d4 g6 2. c4 Bg7 3. Nc3 c5 4. d5 Bxc3+ 5. bxc3 f5

[ECO "A41"]
[Opening "Queen's Pawn"]

1. d4 d6

[ECO "A41"]
[Opening "Old Indian"]
[Variation "Tartakower (Wade) variation"]

1. d4 d6 2. Nf3 Bg4

[ECO "A41"]
[Opening "Old Indian defence"]

1. d4 d6 2. c4

[ECO "A41"]
[Opening "Modern defence"]

1. d4 d6 2. c4 g6 3. Nc3 Bg7

[ECO "A41"]
[Opening "Robatsch defence"]
[Variation "Rossolimo variation"]

1. e4 g6 2. d4 Bg7 3. Nf3 d6 4. c4 Bg4

[ECO "A42"]
[Opening "Modern defence"]
[Variation "Averbakh system"]

1. d4 d6 2. c4 g6 3. Nc3 Bg7 4. e4

[ECO "A42"]
[Opening "Pterodactyl defence"]

1. d4 d6 2. c4 g6 3. Nc3 Bg7 4. e4 c5 5. Nf3 Qa5

[ECO "A42"]
[Opening "Modern defence"]
[Variation "Averbakh system, Randspringer variation"]

1. d4 d6 2. c4 g6 3. Nc3 Bg7 4. e4 f5

[ECO "A42"]
[Opening "Modern defence"]
[Variation "Averbakh system, Kotov variation"]

1. d4 d6 2. c4 g6 3. Nc3 Bg7 4. e4 Nc6

[ECO "A43"]
[Opening "Old Benoni defence"]

1. d4 c5

[ECO "A43"]
[Opening "Old Benoni"]
[Variation "Franco-Benoni defence"]

1. d4 c5 2. d5 e6 3. e4

[ECO "A43"]
[Opening "Old Benoni"]
[Variation "Mujannah formation"]

1. d4 c5 2. d5 f5

[ECO "A43"]
[Opening "Old Benoni defence"]

1. d4 c5 2. d5 Nf6

[ECO "A43"]
[Opening "Woozle defence"]

1. d4 c5 2. d5 Nf6 3. Nc3 Qa5

[ECO "A43"]
[Opening "Old Benoni defence"]

1. d4 c5 2. d5 Nf6 3. Nf3

[ECO "A43"]
[Opening "Hawk (Habichd) defence"]

1. d4 c5 2. d5 Nf6 3. Nf3 c4

[ECO "A43"]
[Opening "Old Benoni defence"]

1. d4 c5 2. d5 d6

[ECO "A43"]
[Opening "Old Benoni"]
[Variation "Schmid's system"]

1. d4 c5 2. d5 d6 3. Nc3 g6

[ECO "A44"]
[Opening "Old Benoni defence"]

1. d4 c5 2. d5 e5

[ECO "A44"]
[Opening "Semi-Benoni (`blockade variation')"]

1. d4 c5 2. d5 e5 3. e4 d6

[ECO "A45"]
[Opening "Queen's pawn game"]

1. d4 Nf6

[ECO "A45"]
[Opening "Queen's pawn"]
[Variation "Bronstein gambit"]

1. d4 Nf6 2. g4

[ECO "A45"]
[Opening "Canard Opening"]

1. d4 Nf6 2. f4

[ECO "A45"]
[Opening "Paleface attack"]

1. d4 Nf6 2. f3

[ECO "A45"]
[Opening "Blackmar-Diemer gambit"]

1. d4 Nf6 2. f3 d5 3. e4

[ECO "A45"]
[Opening "Gedult attack"]

1. d4 Nf6 2. f3 d5 3. g4

[ECO "A45"]
[Opening "Trompovsky attack (Ruth, Opovcensky Opening)"]

1. d4 Nf6 2. Bg5

[ECO "A46"]
[Opening "Queen's pawn game"]

1. d4 Nf6 2. Nf3

[ECO "A46"]
[Opening "Queen's pawn"]
[Variation "Torre attack"]

1. d4 Nf6 2. Nf3 e6 3. Bg5

[ECO "A46"]
[Opening "Queen's pawn"]
[Variation "Torre attack, Wagner gambit"]

1. d4 Nf6 2. Nf3 e6 3. Bg5 c5 4. e4

[ECO "A46"]
[Opening "Queen's pawn"]
[Variation "Yusupov-Rubinstein system"]

1. d4 Nf6 2. Nf3 e6 3. e3

[ECO "A46"]
[Opening "Doery defence"]

1. d4 Nf6 2. Nf3 Ne4

[ECO "A47"]
[Opening "Queen's Indian defence"]

1. d4 Nf6 2. Nf3 b6

[ECO "A47"]
[Opening "Queen's Indian"]
[Variation "Marienbad system"]

1. d4 Nf6 2. Nf3 b6 3. g3 Bb7 4. Bg2 c5

[ECO "A47"]
[Opening "Queen's Indian"]
[Variation "Marienbad system, Berg variation"]

1. d4 Nf6 2. Nf3 b6 3. g3 Bb7 4. Bg2 c5 5. c4 cxd4 6. Qxd4

[ECO "A48"]
[Opening "King's Indian"]
[Variation "East Indian defence"]

1. d4 Nf6 2. Nf3 g6

[ECO "A48"]
[Opening "King's Indian"]
[Variation "Torre attack"]

1. d4 Nf6 2. Nf3 g6 3. Bg5

[ECO "A48"]
[Opening "King's Indian"]
[Variation "London system"]

1. d4 Nf6 2. Nf3 g6 3. Bf4

[ECO "A49"]
[Opening "King's Indian"]
[Variation "Fianchetto without c4"]

1. d4 Nf6 2. Nf3 g6 3. g3

[ECO "A50"]
[Opening "Queen's pawn game"]

1. d4 Nf6 2. c4

[ECO "A50"]
[Opening "Kevitz-Trajkovich defence"]

1. d4 Nf6 2. c4 Nc6

[ECO "A50"]
[Opening "Queen's Indian Accelerated"]

1. d4 Nf6 2. c4 b6

[ECO "A51"]
[Opening "Budapest defence declined"]

1. d4 Nf6 2. c4 e5

[ECO "A51"]
[Opening "Budapest"]
[Variation "Fajarowicz variation"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ne4

[ECO "A51"]
[Opening "Budapest"]
[Variation "Fajarowicz, Steiner variation"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ne4 4. Qc2

[ECO "A52"]
[Opening "Budapest defence"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ng4

[ECO "A52"]
[Opening "Budapest"]
[Variation "Adler variation"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ng4 4. Nf3

[ECO "A52"]
[Opening "Budapest"]
[Variation "Rubinstein variation"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ng4 4. Bf4

[ECO "A52"]
[Opening "Budapest"]
[Variation "Alekhine variation"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ng4 4. e4

[ECO "A52"]
[Opening "Budapest"]
[Variation "Alekhine, Abonyi variation"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ng4 4. e4 Nxe5 5. f4 Nec6

[ECO "A52"]
[Opening "Budapest"]
[Variation "Alekhine variation, Balogh gambit"]

1. d4 Nf6 2. c4 e5 3. dxe5 Ng4 4. e4 d6

[ECO "A53"]
[Opening "Old Indian defence"]

1. d4 Nf6 2. c4 d6

[ECO "A53"]
[Opening "Old Indian"]
[Variation "Janowski variation"]

1. d4 Nf6 2. c4 d6 3. Nc3 Bf5

[ECO "A54"]
[Opening "Old Indian"]
[Variation "Ukrainian variation"]

1. d4 Nf6 2. c4 d6 3. Nc3 e5

[ECO "A54"]
[Opening "Old Indian"]
[Variation "Dus-Khotimirsky variation"]

1. d4 Nf6 2. c4 d6 3. Nc3 e5 4. e3 Nbd7 5. Bd3

[ECO "A54"]
[Opening "Old Indian"]
[Variation "Ukrainian variation, 4.Nf3"]

1. d4 Nf6 2. c4 d6 3. Nc3 e5 4. Nf3

[ECO "A55"]
[Opening "Old Indian"]
[Variation "Main line"]

1. d4 Nf6 2. c4 d6 3. Nc3 e5 4. Nf3 Nbd7 5. e4

[ECO "A56"]
[Opening "Benoni defence"]

1. d4 Nf6 2. c4 c5

[ECO "A56"]
[Opening "Benoni defence, Hromodka system"]

1. d4 Nf6 2. c4 c5 3. d5 d6

[ECO "A56"]
[Opening "Vulture defence"]

1. d4 Nf6 2. c4 c5 3. d5 Ne4

[ECO "A56"]
[Opening "Czech Benoni defence"]

1. d4 Nf6 2. c4 c5 3. d5 e5

[ECO "A56"]
[Opening "Czech Benoni"]
[Variation "King's Indian system"]

1. d4 Nf6 2. c4 c5 3. d5 e5 4. Nc3 d6 5. e4 g6

[ECO "A57"]
[Opening "Benko gambit"]

1. d4 Nf6 2. c4 c5 3. d5 b5

[ECO "A57"]
[Opening "Benko gambit half accepted"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6

[ECO "A57"]
[Opening "Benko gambit"]
[Variation "Zaitsev system"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. Nc3

[ECO "A57"]
[Opening "Benko gambit"]
[Variation "Nescafe Frappe attack"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. Nc3 axb5 6. e4 b4 7. Nb5 d6 8. Bc4

[ECO "A58"]
[Opening "Benko gambit accepted"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. bxa6

[ECO "A58"]
[Opening "Benko gambit"]
[Variation "Nd2 variation"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. bxa6 Bxa6 6. Nc3 d6 7. Nf3 g6 8. Nd2

[ECO "A58"]
[Opening "Benko gambit"]
[Variation "Fianchetto variation"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. bxa6 Bxa6 6. Nc3 d6 7. Nf3 g6 8. g3

[ECO "A59"]
[Opening "Benko gambit"]
[Variation "7.e4"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. bxa6 Bxa6 6. Nc3 d6 7. e4

[ECO "A59"]
[Opening "Benko gambit"]
[Variation "Ne2 variation"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. bxa6 Bxa6 6. Nc3 d6 7. e4 Bxf1 8. Kxf1 g6 9. Nge2

[ECO "A59"]
[Opening "Benko gambit"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. bxa6 Bxa6 6. Nc3 d6 7. e4 Bxf1 8. Kxf1 g6 9. g3

[ECO "A59"]
[Opening "Benko gambit"]
[Variation "Main line"]

1. d4 Nf6 2. c4 c5 3. d5 b5 4. cxb5 a6 5. bxa6 Bxa6 6. Nc3 d6 7. e4 Bxf1 8. Kxf1 g6 9. g3 Bg7 10. Kg2 O-O 11. Nf3

[ECO "A60"]
[Opening "Benoni defence"]

1. d4 Nf6 2. c4 c5 3. d5 e6

[ECO "A61"]
[Opening "Benoni defence"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. Nf3 g6

[ECO "A61"]
[Opening "Benoni"]
[Variation "Uhlmann variation"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. Nf3 g6 7. Bg5

[ECO "A61"]
[Opening "Benoni"]
[Variation "Nimzovich (knight's tour) variation"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. Nf3 g6 7. Nd2

[ECO "A61"]
[Opening "Benoni"]
[Variation "Fianchetto variation"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. Nf3 g6 7. g3

[ECO "A62"]
[Opening "Benoni"]
[Variation "Fianchetto variation"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. Nf3 g6 7. g3 Bg7 8. Bg2 O-O

[ECO "A63"]
[Opening "Benoni"]
[Variation "Fianchetto, 9...Nbd7"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. Nf3 g6 7. g3 Bg7 8. Bg2 O-O 9. O-O Nbd7

[ECO "A64"]
[Opening "Benoni"]
[Variation "Fianchetto, 11...Re8"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. Nf3 g6 7. g3 Bg7 8. Bg2 O-O 9. O-O Nbd7 10. Nd2 a6 11. a4 Re8

[ECO "A65"]
[Opening "Benoni"]
[Variation "6.e4"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4

[ECO "A66"]
[Opening "Benoni"]
[Variation "pawn storm variation"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. f4

[ECO "A66"]
[Opening "Benoni"]
[Variation "Mikenas variation"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. f4 Bg7 8. e5


[ECO "A67"]
[Opening "Benoni"]
[Variation "Taimanov variation"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. f4 Bg7 8. Bb5+

[ECO "A68"]
[Opening "Benoni"]
[Variation "Four pawns attack"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. f4 Bg7 8. Nf3 O-O

[ECO "A69"]
[Opening "Benoni"]
[Variation "Four pawns attack, Main line"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. f4 Bg7 8. Nf3 O-O 9. Be2 Re8

[ECO "A70"]
[Opening "Benoni"]
[Variation "Classical with e4 and Nf3"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3

[ECO "A70"]
[Opening "Benoni"]
[Variation "Classical without 9.O-O"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2

[ECO "A71"]
[Opening "Benoni"]
[Variation "Classical, 8.Bg5"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Bg5

[ECO "A72"]
[Opening "Benoni"]
[Variation "Classical without 9.O-O"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O

[ECO "A73"]
[Opening "Benoni"]
[Variation "Classical, 9.O-O"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O 9. O-O

[ECO "A74"]
[Opening "Benoni"]
[Variation "Classical, 9...a6, 10.a4"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O 9. O-O a6 10. a4

[ECO "A75"]
[Opening "Benoni"]
[Variation "Classical with ...a6 and 10...Bg4"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O 9. O-O a6 10. a4 Bg4

[ECO "A76"]
[Opening "Benoni"]
[Variation "Classical, 9...Re8"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O 9. O-O Re8

[ECO "A77"]
[Opening "Benoni"]
[Variation "Classical, 9...Re8, 10.Nd2"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O 9. O-O Re8 10. Nd2

[ECO "A78"]
[Opening "Benoni"]
[Variation "Classical with ...Re8 and ...Na6"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O 9. O-O Re8 10. Nd2 Na6

[ECO "A79"]
[Opening "Benoni"]
[Variation "Classical, 11.f3"]

1. d4 Nf6 2. c4 c5 3. d5 e6 4. Nc3 exd5 5. cxd5 d6 6. e4 g6 7. Nf3 Bg7 8. Be2 O-O 9. O-O Re8 10. Nd2 Na6 11. f3

[ECO "A80"]
[Opening "Dutch"]

1. d4 f5

[ECO "A80"]
[Opening "Dutch, Spielmann gambit"]

1. d4 f5 2. Nc3 Nf6 3. g4

[ECO "A80"]
[Opening "Dutch, Manhattan (Alapin, Ulvestad) variation"]

1. d4 f5 2. Qd3

[ECO "A80"]
[Opening "Dutch, Von Pretzel gambit"]

1. d4 f5 2. Qd3 e6 3. g4

[ECO "A80"]
[Opening "Dutch, Korchnoi attack"]

1. d4 f5 2. h3

[ECO "A80"]
[Opening "Dutch, Krejcik gambit"]

1. d4 f5 2. g4

[ECO "A80"]
[Opening "Dutch, 2.Bg5 variation"]

1. d4 f5 2. Bg5

[ECO "A81"]
[Opening "Dutch defence"]

1. d4 f5 2. g3

[ECO "A81"]
[Opening "Dutch defence, Blackburne variation"]

1. d4 f5 2. g3 Nf6 3. Bg2 e6 4. Nh3

[ECO "A81"]
[Opening "Dutch defence"]

1. d4 f5 2. g3 Nf6 3. Bg2 g6

[ECO "A81"]
[Opening "Dutch"]
[Variation "Leningrad, Basman system"]

1. d4 f5 2. g3 g6 3. Bg2 Bg7 4. Nf3 c6 5. O-O Nh6

[ECO "A81"]
[Opening "Dutch"]
[Variation "Leningrad, Karlsbad variation"]

1. d4 f5 2. g3 g6 3. Bg2 Bg7 4. Nh3

[ECO "A82"]
[Opening "Dutch"]
[Variation "Staunton gambit"]

1. d4 f5 2. e4

[ECO "A82"]
[Opening "Dutch"]
[Variation "Balogh defence"]

1. d4 f5 2. e4 d6

[ECO "A82"]
[Opening "Dutch"]
[Variation "Staunton gambit"]

1. d4 f5 2. e4 fxe4

[ECO "A82"]
[Opening "Dutch"]
[Variation "Staunton gambit, Tartakower variation"]

1. d4 f5 2. e4 fxe4 3. Nc3 Nf6 4. g4

[ECO "A83"]
[Opening "Dutch"]
[Variation "Staunton gambit, Staunton's line"]

1. d4 f5 2. e4 fxe4 3. Nc3 Nf6 4. Bg5

[ECO "A83"]
[Opening "Dutch"]
[Variation "Staunton gambit, Alekhine variation"]

1. d4 f5 2. e4 fxe4 3. Nc3 Nf6 4. Bg5 g6 5. h4

[ECO "A83"]
[Opening "Dutch"]
[Variation "Staunton gambit, Lasker variation"]

1. d4 f5 2. e4 fxe4 3. Nc3 Nf6 4. Bg5 g6 5. f3

[ECO "A83"]
[Opening "Dutch"]
[Variation "Staunton gambit, Chigorin variation"]

1. d4 f5 2. e4 fxe4 3. Nc3 Nf6 4. Bg5 c6

[ECO "A83"]
[Opening "Dutch"]
[Variation "Staunton gambit, Nimzovich variation"]

1. d4 f5 2. e4 fxe4 3. Nc3 Nf6 4. Bg5 b6

[ECO "A84"]
[Opening "Dutch defence"]

1. d4 f5 2. c4

[ECO "A84"]
[Opening "Dutch defence"]
[Variation "Bladel variation"]

1. d4 f5 2. c4 g6 3. Nc3 Nh6

[ECO "A84"]
[Opening "Dutch defence"]

1. d4 f5 2. c4 e6

[ECO "A84"]
[Opening "Dutch defence, Rubinstein variation"]

1. d4 f5 2. c4 e6 3. Nc3

[ECO "A84"]
[Opening "Dutch"]
[Variation "Staunton gambit deferred"]

1. d4 f5 2. c4 e6 3. e4

[ECO "A84"]
[Opening "Dutch defence"]

1. d4 f5 2. c4 Nf6

[ECO "A85"]
[Opening "Dutch with c4 & Nc3"]

1. d4 f5 2. c4 Nf6 3. Nc3

[ECO "A86"]
[Opening "Dutch with c4 & g3"]

1. d4 f5 2. c4 Nf6 3. g3

[ECO "A86"]
[Opening "Dutch"]
[Variation "Hort-Antoshin system"]

1. d4 f5 2. c4 Nf6 3. g3 d6 4. Bg2 c6 5. Nc3 Qc7

[ECO "A86"]
[Opening "Dutch"]
[Variation "Leningrad variation"]

1. d4 f5 2. c4 Nf6 3. g3 g6

[ECO "A87"]
[Opening "Dutch"]
[Variation "Leningrad, Main variation"]

1. d4 f5 2. c4 Nf6 3. g3 g6 4. Bg2 Bg7 5. Nf3

[ECO "A88"]
[Opening "Dutch"]
[Variation "Leningrad, Main variation with c6"]

1. d4 f5 2. c4 Nf6 3. g3 g6 4. Bg2 Bg7 5. Nf3 O-O 6. O-O d6 7. Nc3 c6

[ECO "A89"]
[Opening "Dutch"]
[Variation "Leningrad, Main variation with Nc6"]

1. d4 f5 2. c4 Nf6 3. g3 g6 4. Bg2 Bg7 5. Nf3 O-O 6. O-O d6 7. Nc3 Nc6

[ECO "A90"]
[Opening "Dutch defence"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2

[ECO "A90"]
[Opening "Dutch defence"]
[Variation "Dutch-Indian (Nimzo-Dutch) variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Bb4+

[ECO "A90"]
[Opening "Dutch-Indian, Alekhine variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Bb4+ 5. Bd2 Be7

[ECO "A91"]
[Opening "Dutch defence"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7

[ECO "A92"]
[Opening "Dutch defence"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O

[ECO "A92"]
[Opening "Dutch defence, Alekhine variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O Ne4

[ECO "A92"]
[Opening "Dutch"]
[Variation "Stonewall variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d5

[ECO "A92"]
[Opening "Dutch"]
[Variation "Stonewall with Nc3"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d5 7. Nc3

[ECO "A93"]
[Opening "Dutch"]
[Variation "Stonewall, Botwinnik variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d5 7. b3

[ECO "A94"]
[Opening "Dutch"]
[Variation "Stonewall with Ba3"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d5 7. b3 c6 8. Ba3


[ECO "A95"]
[Opening "Dutch"]
[Variation "Stonewall with Nc3"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d5 7. Nc3 c6

[ECO "A95"]
[Opening "Dutch"]
[Variation "Stonewall: Chekhover variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d5 7. Nc3 c6 8. Qc2 Qe8 9. Bg5

[ECO "A96"]
[Opening "Dutch"]
[Variation "Classical variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d6

[ECO "A97"]
[Opening "Dutch"]
[Variation "Ilyin-Genevsky variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d6 7. Nc3 Qe8

[ECO "A97"]
[Opening "Dutch"]
[Variation "Ilyin-Genevsky, Winter variation"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d6 7. Nc3 Qe8 8. Re1

[ECO "A98"]
[Opening "Dutch"]
[Variation "Ilyin-Genevsky variation with Qc2"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d6 7. Nc3 Qe8 8. Qc2

[ECO "A99"]
[Opening "Dutch"]
[Variation "Ilyin-Genevsky variation with b3"]

1. d4 f5 2. c4 Nf6 3. g3 e6 4. Bg2 Be7 5. Nf3 O-O 6. O-O d6 7. Nc3 Qe8 8. b3
