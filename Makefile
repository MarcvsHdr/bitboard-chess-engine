.PHONY: main clean run

main: BitBoardMain

run: RunHvsBB

BitBoardMain:
	g++ -o BitBoardMain -O3 ./BitBoard/*.cpp

RunHvsBB: BitBoardMain
	xboard -fcp "./BitBoardMain" -fd "`pwd`" -debug

RunBBvsBB: BitBoardMain
	xboard -matchMode true -fcp "./BitBoardMain" -fd "`pwd`" -scp "./BitBoardMain" -sd "`pwd`" -debug

clean: 
	rm -f BitBoardMain
	rm -f ./xboard.debug
