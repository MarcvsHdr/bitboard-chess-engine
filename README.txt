BitBoard Chess Engine
=====================

AUTHORS:

  Adrian Scoica (adrian.scoica@gmail.com)
  Andrei Lucian Croitoru (andrei.croi@gmail.com)

ABOUT:

The BitBoard Chess Engine AI was written in the first half of 2010 for the
purpose of competing in a Chess Engine tournament at the Politehnica University
of Bucharest. It subsequently won the tournament organized as part of the
Algorithm Design class of the authors.

REQUIREMENTS:

To run the BitBoard Chess Engine, you need to download the C++ source code from
this repository, and compile it. We have so far tested the engine on 32bit and
64bit Debian-based Linux distributions only (eg. Debian, Ubuntu, and Linux Mint).

In Linux, run the following command with no parameters in the root directory
of the project in order to compile the binary:

> make

The engine is designed to run with the GNU XBoard platform[0], and follows the
communication protocol specified in its User Guide[1]. In Debian-based Linux
distributions (which use the APT package manager), you can install XBoard by
opening a terminal and typing:

> sudo apt-get install xboard

RUNNING:

With XBoard, you can run the chess engine either:

(A) interactively (user vs. BitBoard);

> make RunHvsBB

(B) against itself (BitBoard vs. BitBoard);

> make RunBBvsBB

(C) against another Chess engine.

>   xboard -matchMode true -debug \
        -fcp "./BitBoardMain" -fd "`pwd`" \
        -scp "YOUR CHESS ENGINE LAUNCH COMMAND HERE" -sd "PATH FOR COMMAND"

For more options, such as setting match modes and/or switching the players,
please refer to the XBoard man page.

A video tutorial on the compiling and running engine is available at[2].

REFERENCES

[0] - http://www.gnu.org/software/xboard/
[1] - http://www.gnu.org/software/xboard/user_guide/UserGuide.html
[2] - https://www.youtube.com/watch?v=c2yKvhVHg9c
