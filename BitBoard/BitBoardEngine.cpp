# ifndef __BITBOARDTESTER__

# include <iostream>
# include <cstdlib>
# include <string>
# include <fstream>
# include <ctime>
# include <csignal>

# include "BitBoardProtocol.h"
# include "BitBoardThink.h"
# include "BitBoardConstants.h"

using namespace std;

int main(){
  string comanda;

  BitBoardPlayerColor color;
  BitBoardPlayerColor opponentColor;
  BitBoard board, prevboard = board;
  BitBoardHandlers::reset(board);
  color = BB_BLACK;
  opponentColor = BB_WHITE;
  signal(SIGINT,  SIG_IGN);
  signal(SIGTERM, SIG_IGN);
  cout.setf(ios::unitbuf);
  cin.setf(ios::unitbuf);
  string mutare;

  int rez_parseMove;
  srand(time(NULL));

  BitBoardConstantInitializers::initializeAll();

  do{
    cin >> comanda;

    if (comanda == "new") {
      color = BB_BLACK;
      opponentColor = BB_WHITE;
      BitBoardHandlers::reset(board);
      BitBoardDatabase1::iAmHere = BitBoardDatabase1::root;
    } else if (comanda == "protover"){
      cin >> comanda;
      if (comanda == "2") {
        // When the xboard engine sends "protover 2", we might also receive
        // a list of features. For now, we only need SAN. The SAN feature
        // communicates to xboard that we are expecting moves in the SAN format.
        cout << "feature san=1\n";
      }
    } else if (comanda == "xboard") {
      cout << "\n";
    } else if (comanda == "force") {
    } else if (comanda == "go") {
    } else if (comanda == "quit") {
      break;
    } else if (comanda == "white") {
      color = BB_WHITE;
      opponentColor = BB_BLACK;
      BitBoardDatabase1::iAmHere = BitBoardDatabase1::root;
      mutare = BitBoardThinkMethods::bitBoardThinkDatabaseRandom(board, color);
      cout<<mutare;
    } else if (comanda == "black") {
      color = BB_BLACK;
      opponentColor = BB_WHITE;
      BitBoardDatabase1::iAmHere = BitBoardDatabase1::root;
    } else if ( (rez_parseMove = parseMove(comanda, board, opponentColor)) == 1) {
# ifdef __STDOUTDUMP__
      if (color == BB_BLACK) {
        cout << "BEFORE HIS MOVE THIS IS BLACK'S VIEW" << "\n";
      } else {
        cout << "BEFORE HIS MOVE THIS IS WHITE'S VIEW" << "\n";
      }
      BitBoardHandlers::dump(board,0);
      prevboard = board;
# endif
      BitBoardDatabase1::updateCurrentState(comanda);
      mutare = BitBoardThinkMethods::bitBoardThinkDatabaseRandom(board, color);
# ifdef __STDOUTDUMP__
      if (color == BB_BLACK) {
        cout << "AFTER HIS MOVE THIS IS BLACK'S VIEW" << "\n";
      } else {
        cout << "AFTER HIS MOVE THIS IS WHITE'S VIEW" << "\n";
      }
      BitBoardHandlers::dump(board,0);
# endif
      cout << mutare;
    } else {
      if (rez_parseMove == 2) {
        // This means we've lost.
        if (color == BB_WHITE) {
          cout << "0-1\n";
        } else {
          cout << "1-0\n";
        }
      } else if (rez_parseMove == 3) {
        // This means it's a draw.
        cout << "1/2-1/2\n";
      }
    }
  } while(1);

  return 0;
}

# endif
