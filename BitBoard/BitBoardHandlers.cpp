# include "BitBoard.h"
# include <stdio.h>
# include <iostream>
# include "BitBoardProtocol.h"
# include "BitBoardThink.h"

void BitBoardHandlers::dumpINT64(int64 number) {
  int64 cursor;
  unsigned char line, col;
  for (line = 8; line >= 1; line--){
    for (col = 1; col <= 8; col++){
      cursor = ((int64)1 << ((line - 1) * 8 + (col - 1)));
      if (cursor & number) {
        printf("1 ");
      } else {
        printf("0 ");
      }
    }
    printf("\n");
  }
  printf("Alt int:\n");
}

void BitBoardHandlers::reset(BitBoard& board) {

  // Place white pawns on the second byte.
  board.view[BB_PIONI_ALB] = 0x000000000000FF00ull;
  // Place white towers on the first byte.
  board.view[BB_TURNURI_ALB] = 0x0000000000000081ull;
  // Place white bishops on the first byte.
  board.view[BB_NEBUNI_ALB] = 0x0000000000000024ull;
  // Place white knights on the first byte.
  board.view[BB_CAI_ALB] = 0x0000000000000042ull;
  // Place white queen on the first byte.
  board.view[BB_REGINE_ALB] = 0x0000000000000008ull;
  // Place white king on the first byte.
  board.view[BB_REGI_ALB] = 0x0000000000000010ull;

  // Place black pawns on byte 7.
  board.view[BB_PIONI_NEGRU] = 0x00FF000000000000ull;
  // Place black towers on byte 8.
  board.view[BB_TURNURI_NEGRU] = 0x8100000000000000ull;
  // Place black bishops on byte 8.
  board.view[BB_NEBUNI_NEGRU] = 0x2400000000000000ull;
  // Place black knights on byte 8.
  board.view[BB_CAI_NEGRU] = 0x4200000000000000ull;
  // Place black queen on byte 8.
  board.view[BB_REGINE_NEGRU] = 0x0800000000000000ull;
  // Place black king on byte 8.
  board.view[BB_REGI_NEGRU] = 0x1000000000000000ull;

  // Set the flags.
  board.flags = 0x8100000000000081ull;
  board.flagsCastling = 0;
}

void BitBoardHandlers::dump(BitBoard& board, int dumpMove = 0) {
  FILE *g= stdout;
  long int i, j, k; 
  int64 aux;
  char toPrint[] = "PTNBQKptnbqk_";
  fprintf(g,"========\n");
  for (i = 8; i >= 1; i--) {
    for (j = 1;j <= 8; j++) {
      aux = (int64)1 << ((j - 1) + 8 * (i - 1));  
      for (k = BB_PIONI_ALB; k <= BB_REGI_NEGRU; k++)
        if (aux & board.view[k]) {
          break;
        }
      fprintf(g, "%c", toPrint[k]);
    }
    fprintf(g, "\n");
  }
  fprintf(g, "========");
  if (dumpMove) {
    cout << buildMoveString(board);
  }
  fprintf(g, "\n");
  fflush(g);
}

void BitBoardHandlers::forceMove(
    BitBoard& board,
    int64 sourcemask,
    int64 destinationmask,
    BitBoardPlayerColor color,
    BitBoardPieceType type) {
  unsigned char k;
  board.view[type + color * 6] =
      (board.view[type + color * 6] ^ sourcemask ^ destinationmask);
  unsigned char capture = 0;
  for (k = (1 - color) * 6; k < (2 - color) * 6; k++) {
    if ((destinationmask & board.view[k]) != 0) {
      capture = 1;
      board.view[k] ^= destinationmask;
    }
  }
  // All En Pasante flags are 0 in the forced move. They must be reset.
  board.flags = (board.flags & 0xFFFFFF0000FFFFFFull);
  // Byte 2 must contain the source.
  board.flags =
      (board.flags & 0xFFFFFFFFFFFF00FFull) |
      (((int64)(BitBoardPositionHandlers::fromMask(sourcemask)) << 8));
  // Byte three must contain the destination.
  board.flags =
      (board.flags & 0xFFFFFFFFFF00FFFFull) |
      (((int64)(BitBoardPositionHandlers::fromMask(destinationmask)) << 16));
  // Bit 6 must contain the colour of the latest player.
  board.flags = (board.flags & 0xFFFFFFFFFFFFFFDFull) | (((int64)color) << 5);
  // Bit 7 must contain whether a capture has taken place or not.
  if (capture) {
    board.flags |= 0x0000000000000040ull;
  } else {
    board.flags &= 0xFFFFFFFFFFFFFFBFull;
  }
  // Bit 2 must contain whether the second player is in chess.
  if (isInChess(board, color ^ 0x01)) {
    board.flags |= 0x0000000000000002ull;
  } else {
    board.flags &= 0xFFFFFFFFFFFFFFFDull;
  }
  // Bits 3-5 must contain the type of the piece.
  board.flags = (board.flags & 0xFFFFFFFFFFFFFFE3ull) | (((int64)type) << 2);
  // By default, bits 57, 58, 59, 60, 61, and 62 must be 0.
  // We shall consider that there are no promotions or castlings before the
  // forceMove function.
  board.flags &= 0x81FFFFFFFFFFFFFFull;
  // From the castling flags, remove the lack of white and black towers.
  board.flags =
      board.flags & (board.view[BB_TURNURI_ALB] | 0xFFFFFFFFFFFFFF7Eull);
  board.flags =
      board.flags & (board.view[BB_TURNURI_NEGRU] | 0x7EFFFFFFFFFFFFFFull);
}

void BitBoardHandlers::forceMove(BitBoard& board,
                                 int64 sourcemask,
                                 int64 destinationmask) {
  // Only one type of piece can exist at that moment at the source/destination.
  // We shall look for the type of piece.
  BitBoardPieceType k;
  for (k = BB_PIONI_ALB; k <= BB_REGI_NEGRU; k++) {
    if (sourcemask & board.view[k]) {
      break;
    }
  }
  if (k < BB_PlayerGap) {
    BitBoardHandlers::forceMove(board, sourcemask,
                                destinationmask, BB_WHITE, k);
  } else {
    BitBoardHandlers::forceMove(board, sourcemask,
                                destinationmask, BB_BLACK, k - BB_PlayerGap);
  }
}

void BitBoardHandlers::forceMove(BitBoard& board,
                                 BitBoardPosition source,
                                 BitBoardPosition destination) {
  BitBoardHandlers::forceMove(board,
                              BitBoardPositionHandlers::toMask(source),
                              BitBoardPositionHandlers::toMask(destination));
}

void BitBoardHandlers::forceMove(BitBoard& board,
                                 BitBoardPosition source,
                                 BitBoardPosition destination,
                                 BitBoardPlayerColor color,
                                 BitBoardPieceType type) {
  BitBoardHandlers::forceMove(board,
                              BitBoardPositionHandlers::toMask(source),
                              BitBoardPositionHandlers::toMask(destination),
                              color,
                              type);
}

int64 BitBoardHandlers::overlayedPieces(BitBoard& board,
                                        BitBoardPlayerColor color) {
  int64 sol = 0;
  for (BitBoardPieceType k = color * 6; k < (color + 1) * 6; k++) {
    sol |= board.view[k];
  }
  return sol;
}

void BitBoardHandlers::moveKnights(BitBoard& board,
                                   BitBoardPlayerColor color,
                                   BitBoard* nextStates,
                                   int& nextStatesLength) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 knightRepository = board.view[color * 6 + BB_CAL];
  int64 currentKnight;
  int64 nextMovesRepository;
  int64 destinationKnight;
  while (knightRepository) {
    // Remove a knight from the repository.
    currentKnight = RIGHTMOST_BIT(knightRepository);
    knightRepository ^= currentKnight;
    // Built all of its possible future states. There are 8 of them.
    nextMovesRepository = 0;
    nextMovesRepository |= ((currentKnight << 1) & 0xFEFEFEFEFEFEFEFEull) >> 16;
    nextMovesRepository |= ((currentKnight << 1) & 0xFEFEFEFEFEFEFEFEull) << 16;
    nextMovesRepository |= ((currentKnight << 2) & 0xFCFCFCFCFCFCFCFCull) << 8;
    nextMovesRepository |= ((currentKnight << 2) & 0xFCFCFCFCFCFCFCFCull) >> 8;
    nextMovesRepository |= ((currentKnight >> 1) & 0x7F7F7F7F7F7F7F7Full) >> 16;
    nextMovesRepository |= ((currentKnight >> 1) & 0x7F7F7F7F7F7F7F7Full) << 16;
    nextMovesRepository |= ((currentKnight >> 2) & 0x3F3F3F3F3F3F3F3Full) << 8;
    nextMovesRepository |= ((currentKnight >> 2) & 0x3F3F3F3F3F3F3F3Full) >> 8;
    nextMovesRepository &= (~myPieces);
    // Remove the squares which are occupied by self pieces.
    while (nextMovesRepository) {
      nextStates[nextStatesLength] = board;
      destinationKnight = RIGHTMOST_BIT(nextMovesRepository);
      BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                  currentKnight,
                                  destinationKnight,
                                  color,
                                  BB_CAL);
      if (isInChess(nextStates[nextStatesLength],color)) {
        nextStatesLength--;
      }
      nextMovesRepository ^= destinationKnight;
      nextStatesLength++;
    }
  }
}

void BitBoardHandlers::moveBishops(BitBoard& board,
                                   BitBoardPlayerColor color,
                                   BitBoard* nextStates,
                                   int& nextStatesLength) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 bishopRepository = board.view[color * 6 + BB_NEBUN];
  int64 currentBishop;
  int64 nextMovesRepository;
  int64 destinationBishop;
  while (bishopRepository){
    // Remove a bishop from the repository.
    currentBishop = RIGHTMOST_BIT(bishopRepository);
    bishopRepository ^= currentBishop;
    // Build all of its future states. This means:
    // * downwards on the diagonals, until it hits a piece or gets out of the
    //   board.
    // * upwards on the diagonals, until it hits a piece of gets out of the
    //   board.
    nextMovesRepository = 0;
    // A). Upwards and rightwards on the diagonal. This means shifting by 7.
    //     Should not make it to the first column.
    destinationBishop = currentBishop;
    while (destinationBishop) {
      destinationBishop =
          (destinationBishop >> 7) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
      nextMovesRepository |= destinationBishop;
      destinationBishop &= (~hisPieces);
    }
    // B). Upwards and leftwards on the diagonal. This means shifting by 9.
    //     Should not make it to the first column.
    destinationBishop = currentBishop;
    while (destinationBishop) {
      destinationBishop =
          (destinationBishop << 9) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
      nextMovesRepository |= destinationBishop;
      destinationBishop &= (~hisPieces);
    }
    // C). Downwards and leftwards on the diagonal. This means shifting by 7.
    //     Should not make it to the final column.
    destinationBishop = currentBishop;
    while (destinationBishop) {
      destinationBishop =
          (destinationBishop << 7) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
      nextMovesRepository |= destinationBishop;
      destinationBishop &= (~hisPieces);
    }
    // D). Downwards and rightwards on the diagonal. This means shifting by 9.
    //     Should not make it to the final column.
    destinationBishop = currentBishop;
    while (destinationBishop) {
      destinationBishop =
          (destinationBishop >> 9) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
      nextMovesRepository |= destinationBishop;
      destinationBishop &= (~hisPieces);
    }
    // Build the moves from what is left.
    while (nextMovesRepository) {
      nextStates[nextStatesLength]=board;
      destinationBishop = RIGHTMOST_BIT(nextMovesRepository);
      BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                  currentBishop,
                                  destinationBishop,
                                  color,
                                  BB_NEBUN);
      if (isInChess(nextStates[nextStatesLength], color)) {
        nextStatesLength--;
      }
      nextMovesRepository ^= destinationBishop;
      nextStatesLength++;
    }
  }
}

void BitBoardHandlers::moveRooks(BitBoard& board,
                                 BitBoardPlayerColor color,
                                 BitBoard* nextStates,
                                 int& nextStatesLength) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 rookRepository = board.view[color * 6 + BB_TURN];
  int64 currentRook;
  int64 nextMovesRepository;
  int64 destinationRook;
  while (rookRepository){
    // Remove a rook from the repository.
    currentRook = RIGHTMOST_BIT(rookRepository);
    rookRepository ^= currentRook;
    // Build all of its future states. This means:
    // * upwards and downwards on the file, till another own piece or the edge
    //   of the board.
    // * left and right on the rank, till another own piece or the edge of
    //   the board.
    nextMovesRepository = 0;
    // A). Upwards. This means shifting by 8.
    destinationRook = currentRook;
    while (destinationRook) {
      destinationRook = (destinationRook << 8) & (~myPieces);
      nextMovesRepository |= destinationRook;
      destinationRook &= (~hisPieces);
    }
    // B). Downwards. This means shifting by 8.
    destinationRook = currentRook;
    while (destinationRook) {
      destinationRook = (destinationRook >> 8) & (~myPieces);
      nextMovesRepository |= destinationRook;
      destinationRook &= (~hisPieces);
    }
    //C). Leftwards. This means shifting by 1. Should not end up on the final
    //    column.
    destinationRook = currentRook;
    while (destinationRook) {
      destinationRook =
          (destinationRook >> 1) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
      nextMovesRepository |= destinationRook;
      destinationRook &= (~hisPieces);
    }
    // D). Rightward. This means shifting by 1. Should not end up on the final
    //     column.
    destinationRook = currentRook;
    while (destinationRook) {
      destinationRook =
          (destinationRook << 1) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
      nextMovesRepository |= destinationRook;
      destinationRook &= (~hisPieces);
    }
    // Build the moves from what is left.
    while (nextMovesRepository) {
      nextStates[nextStatesLength]=board;
      destinationRook = RIGHTMOST_BIT(nextMovesRepository);
      BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                  currentRook,
                                  destinationRook,
                                  color,
                                  BB_TURN);
      if (isInChess(nextStates[nextStatesLength], color)) {
        nextStatesLength--;
      }
      nextMovesRepository ^= destinationRook;
      nextStatesLength++;
    }
  }
}

void BitBoardHandlers::moveQueens(BitBoard& board,
                                  BitBoardPlayerColor color,
                                  BitBoard* nextStates,
                                  int& nextStatesLength) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 queenRepository = board.view[color * 6 + BB_REGINA];
  int64 currentQueen;
  int64 nextMovesRepository;
  int64 destinationQueen;
  while (queenRepository){
    // Remove a queen from the repository.
    currentQueen = RIGHTMOST_BIT(queenRepository);
    queenRepository ^= currentQueen;
    // Build all of its future states. This means:
    // * upwards until one of its own pieces, or the edge of the board.
    // * downards until one of its own pieces, or the edge of the board.
    // * leftwards until one of its own pieces, or the edge of the board.
    // * rightwards until one of its own pieces, or the edge of the board.
    // * diagonals, likewise.
    nextMovesRepository = 0;
    // A). Upwards. This means shifting by 8.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen = (destinationQueen << 8) & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // B). Downwards. This means shifting by 8.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen = (destinationQueen >> 8) & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // C). Leftwards. This means shifting by 1. Should not end up on the final
    //     column.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen =
          (destinationQueen >> 1) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // D). Rightwards. This means shifting by 1. Should not end up on the first
    //     column.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen =
          (destinationQueen << 1) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // E). Downwards and to the right. This means shifting by 7. Should not end
    //     up on the first column.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen =
          (destinationQueen >> 7) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // F). Upwards and to the right. This means shifting by 9. Should not end up
    //     on the first column.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen =
          (destinationQueen << 9) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // G). Upwards and to the left. This means shifting by 7. Should not end up
    //     on the final column.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen =
          (destinationQueen << 7) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // H). Downwards and to the left. This means shifting by 9. Should not end
    //     up on the final column.
    destinationQueen = currentQueen;
    while (destinationQueen) {
      destinationQueen =
          (destinationQueen >> 9) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
      nextMovesRepository |= destinationQueen;
      destinationQueen &= (~hisPieces);
    }
    // Build the moves from what is left.
    while (nextMovesRepository) {
      nextStates[nextStatesLength] = board;
      destinationQueen = RIGHTMOST_BIT(nextMovesRepository);
      BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                  currentQueen,
                                  destinationQueen,
                                  color,
                                  BB_REGINA);
      if (isInChess(nextStates[nextStatesLength], color)) {
        nextStatesLength--;
      }
      nextMovesRepository ^= destinationQueen;
      nextStatesLength++;
    }
  }
}

void BitBoardHandlers::movePawns(BitBoard& board,
                                 BitBoardPlayerColor color,
                                 BitBoard* nextStates,
                                 int& nextStatesLength) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 allFreeSpaces = (~myPieces) & (~hisPieces);
  int64 pawnRepository = board.view[color * 6 + BB_PION];
  int64 currentPawn;
  int64 destinationPawn;
  while (pawnRepository){
    // Remove a pawn from the repository.
    currentPawn = RIGHTMOST_BIT(pawnRepository);
    pawnRepository ^= currentPawn;
    // Build all of its moves.
    if (color == BB_WHITE){
      // A). Forwards by 1. This means shifting by 8.
      destinationPawn = (currentPawn << 8) & allFreeSpaces;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);
        // Ugrade.
        if (destinationPawn & 0xFF00000000000000ull) {
          BitBoardHandlers::upgradePawn(nextStates[nextStatesLength],
                                        destinationPawn,
                                        color,
                                        BB_REGINA);
        }
        // Check if the board is valid (not in chess).
        if (isInChess(nextStates[nextStatesLength], color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // B). Forward by 2, but only if it's on rank 2 and it's free up ahead.
      destinationPawn =
          (currentPawn << 16) &
          allFreeSpaces &
          (allFreeSpaces<<8) &
          0x00000000FF000000ull;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);

        // Only this pawn can be killed En Pasante.
        nextStates[nextStatesLength].flags =
            (nextStates[nextStatesLength].flags & 0xFFFFFFFF00FFFFFFull) |
            destinationPawn; 
        if (isInChess(nextStates[nextStatesLength],color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // C). Upwards and to the left by 2, if it kills something.
      destinationPawn =
          (currentPawn << 7) &
          (~myPieces) &
          (hisPieces) &
          0x7F7F7F7F7F7F7F7Full;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);
        // Upgrade.
        if (destinationPawn & 0xFF00000000000000ull) {
          BitBoardHandlers::upgradePawn(nextStates[nextStatesLength],
                                        destinationPawn,
                                        color,
                                        BB_REGINA);
        }
        // Check that the board is valid.
        if (isInChess(nextStates[nextStatesLength], color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // D). Upwards and to the left by 2, if it kills something.
      destinationPawn =
          (currentPawn << 9) &
          (~myPieces) &
          (hisPieces) &
          0xFEFEFEFEFEFEFEFEull;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);
        // Upgrade.
        if (destinationPawn & 0xFF00000000000000ull) {
          BitBoardHandlers::upgradePawn(nextStates[nextStatesLength],
                                        destinationPawn,
                                        color,
                                        BB_REGINA);
        }
        // Check that the board is valid.
        if (isInChess(nextStates[nextStatesLength], color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // E). Upwards on a diagonal, with En Pasante killing.
      destinationPawn =
          (currentPawn << 7) & (~myPieces) & 0x7F7F7F7F7F7F7F7Full;
      if (board.view[BB_PIONI_NEGRU] &
          (currentPawn >> 1) &
          0x7F7F7F7F7F7F7F7Full &
          0x000000FF00000000ull &
          board.flags) {
        if (destinationPawn) {
          nextStates[nextStatesLength] = board;
          BitBoardHandlers::enPassante(nextStates[nextStatesLength],
                                       currentPawn,
                                       destinationPawn,
                                       color,
                                       BB_PION);
          if (isInChess(nextStates[nextStatesLength],color)) {
            nextStatesLength--;
          }
          nextStatesLength++;
        }
      }
      // F). Upwards on a diagonal, with En Pasante killing.
      destinationPawn =
          (currentPawn << 9) & (~myPieces) & 0xFEFEFEFEFEFEFEFEull;
      if (board.view[BB_PIONI_NEGRU] &
          (currentPawn << 1) &
          0xFEFEFEFEFEFEFEFEull &
          0x000000FF00000000ull &
          board.flags) {
        if (destinationPawn) {
          nextStates[nextStatesLength] = board;
          BitBoardHandlers::enPassante(nextStates[nextStatesLength],
                                       currentPawn,
                                       destinationPawn,
                                       color,
                                       BB_PION);
          if (isInChess(nextStates[nextStatesLength],color)) {
            nextStatesLength--;
          }
          nextStatesLength++;
        }
      }
    } else{
      // A). Forwards by 1.
      destinationPawn = (currentPawn >> 8) & allFreeSpaces;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);
        // Upgrade.
        if (destinationPawn & 0x00000000000000FFull)
          BitBoardHandlers::upgradePawn(nextStates[nextStatesLength],
                                        destinationPawn,
                                        color,
                                        BB_REGINA);
        // Check that the board is valid.
        if (isInChess(nextStates[nextStatesLength],color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // B). Forwards by 2.
      destinationPawn =
          (currentPawn >> 16) &
          allFreeSpaces &
          (allFreeSpaces>>8) &
          0x000000FF00000000ull;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);
        // Check for En Pasante killing.
        nextStates[nextStatesLength].flags =
            (nextStates[nextStatesLength].flags & 0xFFFFFF00FFFFFFFFull ) |
            destinationPawn; 
        if (isInChess(nextStates[nextStatesLength],color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // C). Diagonal by 2.
      destinationPawn =
          (currentPawn >> 7) &
          (~myPieces) &
          (hisPieces) &
          0xFEFEFEFEFEFEFEFEull;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);
        // Upgrade.
        if (destinationPawn & 0x00000000000000FFull) {
          BitBoardHandlers::upgradePawn(nextStates[nextStatesLength],
                                        destinationPawn,
                                        color,
                                        BB_REGINA);
        }
        // Check that the board is valid.
        if (isInChess(nextStates[nextStatesLength],color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // D). Diagonal by 2 positions.
      destinationPawn =
          (currentPawn >> 9) &
          (~myPieces) &
          (hisPieces) &
          0x7F7F7F7F7F7F7F7Full;
      if (destinationPawn) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                    currentPawn,
                                    destinationPawn,
                                    color,
                                    BB_PION);
        // Upgrade.
        if (destinationPawn & 0x00000000000000FFull) {
          BitBoardHandlers::upgradePawn(nextStates[nextStatesLength],
                                        destinationPawn,
                                        color,
                                        BB_REGINA);
        }
        // Check that the board is valid.
        if (isInChess(nextStates[nextStatesLength],color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // E). Diagonal with En Pasante.
      destinationPawn =
          (currentPawn >> 7) & (~myPieces) & 0xFEFEFEFEFEFEFEFEull;
      if (board.view[BB_PIONI_ALB] &
          (currentPawn << 1) &
          0xFEFEFEFEFEFEFEFEull &
          0x00000000FF000000ull &
          board.flags) {
        if (destinationPawn) {
          nextStates[nextStatesLength] = board;
          BitBoardHandlers::enPassante(nextStates[nextStatesLength],
                                       currentPawn,
                                       destinationPawn,
                                       color,
                                       BB_PION);
          if (isInChess(nextStates[nextStatesLength],color)) {
            nextStatesLength--;
          }
          nextStatesLength++;
        }
      }

      // F). Diagonal with En Pasante.
      destinationPawn =
          (currentPawn >> 9) & (~myPieces) & 0x7F7F7F7F7F7F7F7Full; 
      if (board.view[BB_PIONI_ALB] &
          (currentPawn >> 1) &
          0x7F7F7F7F7F7F7F7Full &
          0x00000000FF000000ull &
          board.flags) {
        if (destinationPawn) {
          nextStates[nextStatesLength] = board;
          BitBoardHandlers::enPassante(nextStates[nextStatesLength],
                                       currentPawn,
                                       destinationPawn,
                                       color,
                                       BB_PION);
          if (isInChess(nextStates[nextStatesLength], color)) {
            nextStatesLength--;
          }
          nextStatesLength++;
        }
      }
    }
  }
}

void BitBoardHandlers::moveKings(BitBoard& board,
                                 BitBoardPlayerColor color,
                                 BitBoard* nextStates,
                                 int& nextStatesLength) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 currentKing = board.view[color * 6 + BB_REGE];
  int64 nextMovesRepository;
  int64 destinationKing;

  // Build its future states. There are 8 of them in total.
  nextMovesRepository = 0;
  // A). Right.
  nextMovesRepository |= (currentKing << 1) & 0xFEFEFEFEFEFEFEFEull;
  // B). Left.
  nextMovesRepository |= (currentKing >> 1) & 0x7F7F7F7F7F7F7F7Full;
  // C). Up.
  nextMovesRepository |= (currentKing << 8);
  // D). Down.
  nextMovesRepository |= (currentKing >> 8);
  // E). Up and Left.
  nextMovesRepository |= (currentKing << 7) & 0x7F7F7F7F7F7F7F7Full;
  // F). Up and Right.
  nextMovesRepository |= (currentKing << 9) & 0xFEFEFEFEFEFEFEFEull;
  // G). Down and right.
  nextMovesRepository |= (currentKing >> 7) & 0xFEFEFEFEFEFEFEFEull;
  // H). Down and left.
  nextMovesRepository |= (currentKing >> 9) & 0x7F7F7F7F7F7F7F7Full;
  nextMovesRepository &= (~myPieces);
  // Remove locations occupied by own pieces.
  while (nextMovesRepository) {
    nextStates[nextStatesLength] = board;
    destinationKing = RIGHTMOST_BIT(nextMovesRepository);
    BitBoardHandlers::forceMove(nextStates[nextStatesLength],
                                currentKing,
                                destinationKing,
                                color,
                                BB_REGE);
    // Clear the castling flags for the color.
    if (color == BB_WHITE) {
      nextStates[nextStatesLength].flags &= 0xFFFFFFFFFFFFFF7Eull;
    } else {
      nextStates[nextStatesLength].flags &= 0x7EFFFFFFFFFFFFFFull;
    }
    if (isInChess(nextStates[nextStatesLength], color)) {
      nextStatesLength--;
    }
    nextMovesRepository ^= destinationKing;
    nextStatesLength++;
  }

  // Perform castling here. Castling is not allowed in check.
  if (!isInChess(board, color)) {
    if (color == BB_WHITE) {
      // Catling to the left with the white king.
      if ((board.flags & 0x0000000000000001ull) &&
          !isThreatenedByHim(board, color, 0x000000000000000Cull) &&
          ((myPieces | hisPieces) & 0x000000000000000Eull) == 0) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::castleKing(nextStates[nextStatesLength],
                                     color,
                                     BB_QueenSideCastling);
        if (isInChess(nextStates[nextStatesLength], color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // Castling to the right with the white king.
      if ((board.flags & 0x0000000000000080ull) &&
          !isThreatenedByHim(board, color, 0x0000000000000060ull) &&
          ((myPieces | hisPieces) & 0x0000000000000060ull) == 0) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::castleKing(nextStates[nextStatesLength],
                                     color,
                                     BB_KingSideCastling);
        if (isInChess(nextStates[nextStatesLength], color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
    } else {
      // Castling to the left with the black king.
      if ((board.flags & 0x0100000000000000ull) &&
          !isThreatenedByHim(board, color, 0x0C00000000000000ull) &&
          ((myPieces | hisPieces) & 0x0E00000000000000ull) == 0) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::castleKing(nextStates[nextStatesLength],
                                     color,
                                     BB_QueenSideCastling);
        if (isInChess(nextStates[nextStatesLength], color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
      // Castling to the right with the black king.
      if ((board.flags & 0x8000000000000000ull) &&
          !isThreatenedByHim(board, color, 0x6000000000000000ull) &&
          ((myPieces | hisPieces) & 0x6000000000000000ull) == 0) {
        nextStates[nextStatesLength] = board;
        BitBoardHandlers::castleKing(nextStates[nextStatesLength],
                                     color,
                                     BB_KingSideCastling);
        if (isInChess(nextStates[nextStatesLength], color)) {
          nextStatesLength--;
        }
        nextStatesLength++;
      }
    }
  }
}

int64 BitBoardHandlers::threatenedByKnights(BitBoard& board,
                                            BitBoardPlayerColor color,
                                            int64 thisKnight = 0) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 knightRepository;
  if (thisKnight == 0) {
    knightRepository = board.view[color * 6 + BB_CAL];
  } else {
    knightRepository = thisKnight;
  }
  int64 nextMovesRepository;
  nextMovesRepository = 0;
  nextMovesRepository |= ((knightRepository << 1) & 0xFEFEFEFEFEFEFEFEull) >> 16;
  nextMovesRepository |= ((knightRepository << 1) & 0xFEFEFEFEFEFEFEFEull) << 16;
  nextMovesRepository |= ((knightRepository << 2) & 0xFCFCFCFCFCFCFCFCull) << 8;
  nextMovesRepository |= ((knightRepository << 2) & 0xFCFCFCFCFCFCFCFCull) >> 8;
  nextMovesRepository |= ((knightRepository >> 1) & 0x7F7F7F7F7F7F7F7Full) >> 16;
  nextMovesRepository |= ((knightRepository >> 1) & 0x7F7F7F7F7F7F7F7Full) << 16;
  nextMovesRepository |= ((knightRepository >> 2) & 0x3F3F3F3F3F3F3F3Full) << 8;
  nextMovesRepository |= ((knightRepository >> 2) & 0x3F3F3F3F3F3F3F3Full) >> 8;
  nextMovesRepository &= (~myPieces);
  // Remove the squares occupied by own pieces.
  return nextMovesRepository;
}

int64 BitBoardHandlers::threatenedByBishops(BitBoard& board,
                                            BitBoardPlayerColor color,
                                            int64 thisBishop = 0) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 bishopRepository;
  if (thisBishop == 0) {
    bishopRepository = board.view[color * 6 + BB_NEBUN];
  } else {
    bishopRepository = thisBishop;
  }
  int64 nextMovesRepository;
  int64 destinationBishop;

  nextMovesRepository = 0;
  // A). Upwards and right.
  destinationBishop = bishopRepository;
  while (destinationBishop) {
    destinationBishop =
        (destinationBishop >> 7) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
    nextMovesRepository |= destinationBishop;
    destinationBishop &= (~hisPieces);
  }
  // B). Downwards and right.
  destinationBishop = bishopRepository;
  while (destinationBishop) {
    destinationBishop =
        (destinationBishop << 9) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
    nextMovesRepository |= destinationBishop;
    destinationBishop &= (~hisPieces);
  }
  // C). Upwards and left.
  destinationBishop = bishopRepository;
  while (destinationBishop) {
    destinationBishop =
        (destinationBishop << 7) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
    nextMovesRepository |= destinationBishop;
    destinationBishop &= (~hisPieces);
  }
  // D). Downwards and right.
  destinationBishop = bishopRepository;
  while (destinationBishop) {
    destinationBishop =
        (destinationBishop >> 9) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
    nextMovesRepository |= destinationBishop;
    destinationBishop &= (~hisPieces);
  }
  return nextMovesRepository;
}

int64 BitBoardHandlers::threatenedByRooks(BitBoard& board,
                                          BitBoardPlayerColor color,
                                          int64 thisRook = 0) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 rookRepository;
  if (thisRook == 0) {
    rookRepository = board.view[color * 6 + BB_TURN];
  } else {
    rookRepository = thisRook;
  }
  int64 nextMovesRepository;
  int64 destinationRook;

  nextMovesRepository = 0;
  // A). Upwards.
  destinationRook = rookRepository;
  while (destinationRook) {
    destinationRook = (destinationRook << 8) & (~myPieces);
    nextMovesRepository |= destinationRook;
    destinationRook &= (~hisPieces);
  }
  // B). Downwards.
  destinationRook = rookRepository;
  while (destinationRook) {
    destinationRook = (destinationRook >> 8) & (~myPieces);
    nextMovesRepository |= destinationRook;
    destinationRook &= (~hisPieces);
  }
  // C). Leftwards.
  destinationRook = rookRepository;
  while (destinationRook) {
    destinationRook =
        (destinationRook >> 1) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
    nextMovesRepository |= destinationRook;
    destinationRook &= (~hisPieces);
  }
  // D). Rightwards.
  destinationRook = rookRepository;
  while (destinationRook) {
    destinationRook =
        (destinationRook << 1) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
    nextMovesRepository |= destinationRook;
    destinationRook &= (~hisPieces);
  }

  return nextMovesRepository;
}

int64 BitBoardHandlers::threatenedByQueens(BitBoard& board,
                                           BitBoardPlayerColor color,
                                           int64 thisQueen = 0) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 queenRepository;
  if (thisQueen == 0) {
    queenRepository = board.view[color * 6 + BB_REGINA];
  } else {
    queenRepository = thisQueen;
  }
  int64 nextMovesRepository;
  int64 destinationQueen;

  nextMovesRepository = 0;
  // A). Upwards.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen = (destinationQueen << 8) & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }
  // B). Downwards.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen = (destinationQueen >> 8) & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }
  // C). Leftwards.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen =
        (destinationQueen >> 1) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }
  // D). Rightwards.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen =
        (destinationQueen << 1) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }
  // E). Upwards and right.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen =
        (destinationQueen >> 7) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }
  // F). Downwards and right.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen =
        (destinationQueen << 9) & 0xFEFEFEFEFEFEFEFEull & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }
  // G). Upwards and left.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen =
        (destinationQueen << 7) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }
  // H). Downwards and left.
  destinationQueen = queenRepository;
  while (destinationQueen) {
    destinationQueen =
        (destinationQueen >> 9) & 0x7F7F7F7F7F7F7F7Full & (~myPieces);
    nextMovesRepository |= destinationQueen;
    destinationQueen &= (~hisPieces);
  }

  return nextMovesRepository;
}

int64 BitBoardHandlers::threatenedByPawns(BitBoard& board,
                                          BitBoardPlayerColor color,
                                          int64 thisPawn = 0){
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 pawnRepository;
  if (thisPawn == 0) {
    pawnRepository = board.view[color * 6 + BB_PION];
  } else {
    pawnRepository = thisPawn;
  }

  int64 nextMovesRepository;

  nextMovesRepository = 0;
  if (color == BB_WHITE) {
    nextMovesRepository |=
        (pawnRepository << 7) & (~myPieces) & 0x7F7F7F7F7F7F7F7Full;
    nextMovesRepository |=
        (pawnRepository << 9) & (~myPieces) & 0xFEFEFEFEFEFEFEFEull;
  } else{
    nextMovesRepository |=
        (pawnRepository >> 7) & (~myPieces) & 0xFEFEFEFEFEFEFEFEull;
    nextMovesRepository |=
        (pawnRepository >> 9) & (~myPieces) & 0x7F7F7F7F7F7F7F7Full;
  }

  return nextMovesRepository;
}

int64 BitBoardHandlers::availableToPawns(BitBoard& board,
                                         BitBoardPlayerColor color,
                                         int64 thisPawn = 0) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)color ^ 1);
  int64 allFreeSpaces = (~myPieces) & (~hisPieces);
  int64 pawnRepository;
  if (thisPawn == 0) {
    pawnRepository = board.view[color * 6 + BB_PION];
  } else {
    pawnRepository = thisPawn;
  }

  int64 nextMovesRepository;

  nextMovesRepository = 0;
  if (color == BB_WHITE) {
    nextMovesRepository |= (pawnRepository << 8) & allFreeSpaces;
    nextMovesRepository |=
        (pawnRepository << 16) &
        allFreeSpaces &
        (allFreeSpaces<<8) &
        0x00000000FF000000ull;
    nextMovesRepository |=
        (pawnRepository << 7) &
        (~myPieces) &
        (hisPieces) &
        0x7F7F7F7F7F7F7F7Full;
    nextMovesRepository |=
        (pawnRepository << 9) &
        (~myPieces) &
        (hisPieces) &
        0xFEFEFEFEFEFEFEFEull;
  } else {
    nextMovesRepository |=
        (pawnRepository >> 8) & allFreeSpaces;
    nextMovesRepository |=
        (pawnRepository >> 16) &
        allFreeSpaces &
        (allFreeSpaces>>8) &
        0x000000FF00000000ull;
    nextMovesRepository |=
        (pawnRepository >> 7) &
        (~myPieces) &
        (hisPieces) &
        0xFEFEFEFEFEFEFEFEull;
    nextMovesRepository |=
        (pawnRepository >> 9) &
        (~myPieces) &
        (hisPieces) &
        0x7F7F7F7F7F7F7F7Full;
  }

  return nextMovesRepository;
}

void BitBoardHandlers::upgradePawn(BitBoard& board,
                                   int64 destination,
                                   BitBoardPlayerColor color,
                                   BitBoardPieceType upgradeType) {
  // Remove the pawn.
  board.view[color * 6 + BB_PION] ^= destination;

  // Place the right piece back.
  switch (upgradeType) {
    case BB_REGINA:
      board.view[color * 6 + BB_REGINA] ^= destination;
      break;
    case BB_NEBUN:
      board.view[color * 6 + BB_NEBUN] ^= destination;
      break;
    case BB_TURN:
      board.view[color * 6 + BB_TURN] ^= destination;
      break;
    case BB_CAL:
      board.view[color * 6 + BB_CAL] ^= destination;
      break;
  }

  // Announce that an upgrade has taken place.
  board.flags |= 0x1000000000000000ull;

  // Announce which type of piece was upgraded to.
  board.flags =
      (board.flags & 0xF1FFFFFFFFFFFFFFull) | ((int64) upgradeType << 57);

  // Check whether it is in check, so that we can add a "+" again.
  if (BitBoardHandlers::isInChess(board, color ^ 1)) {
    board.flags |= 0x0000000000000002ull;
  } else {
    board.flags &= 0xFFFFFFFFFFFFFFFDull;
  }
}

void BitBoardHandlers::castleKing(BitBoard& board,
                                  BitBoardPlayerColor color,
                                  BitBoardCastlingType type) {
  if (color == BB_WHITE) {
    if (type == BB_KingSideCastling) {
      // Move the tower from the extreme right, two squares to the left.
      BitBoardHandlers::forceMove(board,
                                  0x0000000000000080ull,
                                  0x0000000000000020ull,
                                  color,
                                  BB_TURN);
      // Move the white king two squares to the right.
      BitBoardHandlers::forceMove(board,
                                  0x0000000000000010ull,
                                  0x0000000000000040ull,
                                  color,
                                  BB_REGE);
    } else {
      // Move the tower from the extreme left, two squares to the right.
      BitBoardHandlers::forceMove(board,
                                  0x0000000000000001ull,
                                  0x0000000000000008ull,
                                  color,
                                  BB_TURN);
      // Move the white king two squares to the left.
      BitBoardHandlers::forceMove(board,
                                  0x0000000000000010ull,
                                  0x0000000000000004ull,
                                  color,
                                  BB_REGE);
    }
    // From now on, white cannot castle again.
    board.flags &= 0xFFFFFFFFFFFFFF7Eull;
    // Say that this king has castled.
    board.flagsCastling |= 0x01;
  } else {
    if (type == BB_KingSideCastling){
      // Move the tower from the extreme right, two squares to the left.
      BitBoardHandlers::forceMove(board,
                                  0x8000000000000000ull,
                                  0x2000000000000000ull,
                                  color,
                                  BB_TURN);
      // Move the white king two squares to the right.
      BitBoardHandlers::forceMove(board,
                                  0x1000000000000000ull,
                                  0x4000000000000000ull,
                                  color,
                                  BB_REGE);
    } else {
      // Move the tower from the extreme left three positions to the right.
      BitBoardHandlers::forceMove(board,
                                  0x0100000000000000ull,
                                  0x0800000000000000ull,
                                  color,
                                  BB_TURN);
      // Move the white king two positions to the left.
      BitBoardHandlers::forceMove(board,
                                  0x1000000000000000ull,
                                  0x0400000000000000ull,
                                  color,
                                  BB_REGE);
    }
    // From now on, black cannot castle again.
    board.flags &= 0x7EFFFFFFFFFFFFFFull;
    // Say that this king has castled.
    board.flagsCastling |= 0x02;
  }
  // Set the castling flags.
  board.flags |= 0x2000000000000000ull; // Castling has taken place.
  board.flags =
      (board.flags & 0xBFFFFFFFFFFFFFFFull) | ( ((int64)type) << 62);
}

void BitBoardHandlers::enPassante(BitBoard& board,
                                  int64 sourcemask,
                                  int64 destinationmask,
                                  BitBoardPlayerColor color,
                                  BitBoardPieceType type) {
  // An En Pasante killing move is similar to a normal pawn movement, only:
  // * there is also a killing of the pawn which lives:
  //     ** above the destination (color == black)
  //     ** below the destination (color == white)
  // * we have to set the capture flag to 1
  // These actions have to be done after the forceMove call, otherwise they will
  // be overwritten by the forceMove call.

  BitBoardHandlers::forceMove(board, sourcemask, destinationmask, color, type);

  if (color == BB_WHITE) {
    board.view[BB_PIONI_NEGRU] ^= destinationmask >> 8;
  } else {
    board.view[BB_PIONI_ALB] ^= destinationmask << 8;
  }

  // Set the capture flag.
  board.flags |= 0x0000000000000040ull;
}

int64 BitBoardHandlers::threatenedByKings(BitBoard& board,
                                          BitBoardPlayerColor color,
                                          int64 thisKing = 0) {
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, color);
  int64 kingRepository; 
  if (thisKing == 0) {
    kingRepository = board.view[color * 6 + BB_REGE];
  } else {
    kingRepository = thisKing;
  }
  int64 nextMovesRepository;

  nextMovesRepository = 0;

  // A). Upwards.
  nextMovesRepository |= (kingRepository << 8) & (~myPieces);
  // B). Downwards.
  nextMovesRepository |= (kingRepository >> 8) & (~myPieces);
  // C). Leftwards.
  nextMovesRepository |=
      (kingRepository >> 1) & (~myPieces) & 0x7F7F7F7F7F7F7F7Full;
  // D). Rightwards.
  nextMovesRepository |=
      (kingRepository << 1) & (~myPieces) & 0xFEFEFEFEFEFEFEFEull;
  // E). Upwards and leftwards.
  nextMovesRepository |=
      (kingRepository << 7) & (~myPieces) & 0x7F7F7F7F7F7F7F7Full;
  // F). Upwards and rightwards.
  nextMovesRepository |=
      (kingRepository << 9) & (~myPieces) & 0xFEFEFEFEFEFEFEFEull;
  // G). Downwards and rightwards.
  nextMovesRepository |=
      (kingRepository >> 7) & (~myPieces) & 0xFEFEFEFEFEFEFEFEull;
  // H). Downwards and leftwards.
  nextMovesRepository |=
      (kingRepository >> 9) & (~myPieces) & 0x7F7F7F7F7F7F7F7Full;

  return nextMovesRepository;
}

bool BitBoardHandlers::isThreatenedByHim(BitBoard& board,
                                         BitBoardPlayerColor color,
                                         int64 position) {
  if (threatenedByKings(board, (char)color ^ 1) & position) {
    return true;
  }
  if (threatenedByPawns(board, (char)color ^ 1) & position) {
    return true;
  }
  if (threatenedByQueens(board, (char)color ^ 1) & position) {
    return true;
  }
  if (threatenedByRooks(board, (char)color ^ 1) & position) {
    return true;
  }
  if (threatenedByBishops(board, (char)color ^ 1) & position) {
    return true;
  }
  if (threatenedByKnights(board, (char)color ^ 1) & position) {
    return true;
  }
  return false;
}

bool BitBoardHandlers::isInChess(BitBoard& board,
                                 BitBoardPlayerColor color) {
  return BitBoardHandlers::isThreatenedByHim(board,
                                             color,
                                             board.view[color * 6 + BB_REGE]);
}

bool BitBoardHandlers::isProtected(BitBoard& board,
                                   BitBoardPlayerColor color,
                                   int64 position) {
  // Change piece color.
  BitBoardPieceType pieceType;
  for (pieceType = BB_PION; pieceType <= BB_REGE; pieceType++) {
    if (position & board.view[pieceType + BB_PlayerGap * (color)]) {
      board.view[pieceType + BB_PlayerGap * (color)] ^= position;
      board.view[pieceType + BB_PlayerGap * ((char)color ^ 1)] ^= position;
      break;
    }
  }
  bool answer =
      BitBoardHandlers::isThreatenedByHim(board, (char)color ^ 1, position);
  board.view[pieceType + BB_PlayerGap * (color)] ^= position;
  board.view[pieceType + BB_PlayerGap * ((char)color ^ 1)] ^= position;
  return answer;
}

int BitBoardHandlers::threatIndex(BitBoard& board,
                                  BitBoardPlayerColor color,
                                  int64 position) {
  int theIndex = 0;
  int64 repository;
  // Kings.
  if (threatenedByKings(board,
                        color,
                        board.view[BB_REGE + BB_PlayerGap * color])) {
    theIndex++;
  }
  // Pawns.
  for (repository = board.view[BB_PION + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theIndex++;
    }
  }
  // Queens.
  for (repository = board.view[BB_REGINA + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theIndex++;
    }
  }
  // Towers.
  for (repository = board.view[BB_TURN + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theIndex++;
    }
  }
  // Bishops.
  for (repository = board.view[BB_NEBUN + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theIndex++;
    }
  }
  // Knights.
  for (repository = board.view[BB_CAL + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theIndex++;
    }
  }
  return theIndex;
}

int BitBoardHandlers::threatValue(BitBoard& board,
                                  BitBoardPlayerColor color,
                                  int64 position) {
  // Check how many pieces attack each square, by piece type.
  int theValue = 0;
  int64 repository;
  // Kings.
  if (threatenedByKings(board,
                        color,
                        board.view[BB_REGE + BB_PlayerGap * color])) {
    //theIndex += BitBoardEvaluationHandlers::
    //    standardValue[BitBoardEvaluationHandlers::standardValueSystem][BB_REGE];
  }
  // Pawns.
  for (repository = board.view[BB_PION + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theValue += BitBoardEvaluationHandlers::
          standardValue[BitBoardEvaluationHandlers::standardValueSystem][BB_PION];
    }
  }
  // Queens.
  for (repository = board.view[BB_REGINA + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theValue += BitBoardEvaluationHandlers::
          standardValue[BitBoardEvaluationHandlers::standardValueSystem][BB_REGINA];
    }
  }
  // Towers.
  for (repository = board.view[BB_TURN + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theValue += BitBoardEvaluationHandlers::
          standardValue[BitBoardEvaluationHandlers::standardValueSystem][BB_TURN];
    }
  }
  // Bishops.
  for (repository = board.view[BB_NEBUN + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theValue += BitBoardEvaluationHandlers::
          standardValue[BitBoardEvaluationHandlers::standardValueSystem][BB_NEBUN];
    }
  }
  // Knights.
  for (repository = board.view[BB_CAL + BB_PlayerGap * color];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    if (threatenedByPawns(board, color, RIGHTMOST_BIT(repository))) {
      theValue += BitBoardEvaluationHandlers::
          standardValue[BitBoardEvaluationHandlers::standardValueSystem][BB_CAL];
    }
  }
  return theValue;
}

int BitBoardHandlers::protectionIndex(BitBoard& board,
                                      BitBoardPlayerColor color,
                                      int64 position) {
  // Change piece color.
  BitBoardPieceType pieceType;
  for (pieceType = BB_PION; pieceType <= BB_REGE; pieceType++){
    if (position & board.view[pieceType + BB_PlayerGap * (color)]) {
      board.view[pieceType + BB_PlayerGap * (color)] ^= position;
      board.view[pieceType + BB_PlayerGap * ((char)color ^ 1)] ^= position;
      break;
    }
  }
  // See if it is attacked now.
  int theIndex = BitBoardHandlers::threatIndex(board, color, position);
  // Change back piece color.
  board.view[pieceType + BB_PlayerGap * (color)] ^= position;
  board.view[pieceType + BB_PlayerGap * ((char)color ^ 1)] ^= position;
  return theIndex;
}

int BitBoardHandlers::protectionValue(BitBoard& board,
                                      BitBoardPlayerColor color,
                                      int64 position) {
  // Change piece color.
  BitBoardPieceType pieceType;
  for (pieceType = BB_PION; pieceType <= BB_REGE; pieceType++) {
    if (position & board.view[pieceType + BB_PlayerGap * (color)]) {
      board.view[pieceType + BB_PlayerGap * (color)] ^= position;
      board.view[pieceType + BB_PlayerGap * ((char)color ^ 1)] ^= position;
      break;
    }
  }
  // See if it is attacked now.
  int theValue = BitBoardHandlers::threatValue(board, color, position);
  // Change back piece color.
  board.view[pieceType + BB_PlayerGap * (color)] ^= position;
  board.view[pieceType + BB_PlayerGap * ((char)color ^ 1)] ^= position;
  return theValue;
}
