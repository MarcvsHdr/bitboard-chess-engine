# ifndef __BITBOARDTHINK_H__
# define __BITBOARDTHINK_H__

# include "BitBoard.h"
# include <string>

using namespace std;

typedef struct minimaxresult{
  BitBoard move;
  int score;
}minimaxResult;

class BitBoardEvaluationHandlers{

 private:
  class Bonuses{

   public:
    static const int knightPositionalBonus[9][9];
    static const int bishopPositionalBonus[9][9];
    static const int pawnPositionalBonus[9];
    static const int pawnAdvancementBonus[9];

    inline static int mobilityBonus(BitBoard&,
                                    int64,
                                    BitBoardPieceType,
                                    BitBoardPlayerColor);
    inline static int positionBonus(BitBoard&,
                                    int64,
                                    BitBoardPieceType,
                                    BitBoardPlayerColor);
    inline static int fearBonus(BitBoard&,
                                int64,
                                BitBoardPieceType,
                                BitBoardPlayerColor);
    inline static int specialBonus(BitBoard&,
                                   int64,
                                   BitBoardPieceType,
                                   BitBoardPlayerColor);
    inline static int allKnightsPositionBonus(BitBoard&, BitBoardPlayerColor);
    inline static int allRooksPositionBonus(BitBoard&, BitBoardPlayerColor);
    inline static int allBishopsPositionBonus(BitBoard&, BitBoardPlayerColor);
    inline static int allPiecesPositionBonus(BitBoard&, BitBoardPlayerColor);
  };

  static const int standardValueSystemCount = 15;
  static const int standardValue[15][5];
  static int standardValueSystem;
 public:
  // Handler for changing the standard value system for the pieces (default 0).
  static void setStandardValueSystem(int);

  // The board evaluation function type, which receives two parameters:
  // the value for the "white" player, and the value for the "black" player.
  typedef void (*BitBoardEvaluationFunction)(BitBoard&, int&, int&);

  // Various board evaluation functions.
  static void materialEvaluation(BitBoard&, int&, int&);
  static void fancyGNUEvaluation(BitBoard&, int&, int&);

  typedef int (*BitBoardComparissonFunction)(const void*, const void*);

  static int  boardComparissonForWhite(const void*, const void*);
  static int  boardComparissonForBlack(const void*, const void*);

  friend class BitBoardHandlers;
};

class BitBoardThinkMethods{

 public:
  inline static bool isQuietNode();

  static string bitBoardThink(BitBoard&, BitBoardPlayerColor);
  static minimaxResult bitBoardMiniMax(
      int,
      BitBoard&,
      BitBoardPlayerColor,
      BitBoardEvaluationHandlers::BitBoardEvaluationFunction);
  static minimaxResult bitBoardAlfaBeta(
      int,
      BitBoard&,
      BitBoardPlayerColor,
      minimaxResult&,
      BitBoardEvaluationHandlers::BitBoardEvaluationFunction);
  static minimaxResult bitBoardQuiescent(
      int,
      BitBoard&,
      BitBoardPlayerColor,
      minimaxResult&,
      BitBoardEvaluationHandlers::BitBoardEvaluationFunction);
  static string bitBoardThinkDatabaseRandom(BitBoard&, BitBoardPlayerColor);
  static string bitBoardThinkRandom(BitBoard&, BitBoardPlayerColor);
};

# endif
