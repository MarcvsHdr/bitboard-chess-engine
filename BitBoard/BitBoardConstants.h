# ifndef __BITBOARDCONSTANTS_H__
# define __BITBOARDCONSTANTS_H__

# include "BitBoard.h"
# include <vector>

using namespace std;

// The following two constant arrays are exceptional in that they have been
// defined in the global namespace.
// All other constant arrays declared solely in the BitBoardConstants class.
const int64 wholecolumn[9] = {
  0x0000000000000000ull,
  0x0101010101010101ull,
  0x0202020202020202ull,
  0x0404040404040404ull,
  0x0808080808080808ull,
  0x1010101010101010ull,
  0x2020202020202020ull,
  0x4040404040404040ull,
  0x8080808080808080ull
};

const int64 wholeline[9] = {
  0x0000000000000000ull,
  0x00000000000000FFull,
  0x000000000000FF00ull,
  0x0000000000FF0000ull,
  0x00000000FF000000ull,
  0x000000FF00000000ull,
  0x0000FF0000000000ull,
  0x00FF000000000000ull,
  0xFF00000000000000ull
};

class BitBoardConstants{
 public:
  static int bitCount[1<<16];
  static int bitPosition[1<<16];
};

// Database with known opening chess moves.
class BitBoardDatabase1{
 public:
  class BitBoardDatabaseNode{
   public:
    string roadFromParent;
    vector< BitBoardDatabaseNode* > nextMove;

    BitBoardDatabaseNode(string);
    ~BitBoardDatabaseNode();

    BitBoardDatabaseNode* addChild(string);
  };

  static BitBoardDatabase1::BitBoardDatabaseNode* root;
  static BitBoardDatabase1::BitBoardDatabaseNode* iAmHere;

  // Initializes the openings database.
  static void loadFromFile(const char*);
  static void initialize();

  // Updates the openings database.
  static void updateCurrentState(string);
  static string suggestNextMove();

  // Frees up the space taken up by the database.
  static void destroy();

  // Debugging functions.
  static void dump(BitBoardDatabaseNode*, FILE*, int);
  static void dumpToFile(const char*);
};

namespace BitBoardConstantInitializers{

void initializeBitCount();
void initializeBitPosition();
void initializeAll();

};

# endif
