# include "BitBoardConstants.h"
# include <stdio.h>
# include <string.h>
# include <cstdlib>

using namespace std;

int BitBoardConstants::bitCount[1 << 16];

int BitBoardConstants::bitPosition[1 << 16];

void BitBoardConstantInitializers::initializeBitPosition() {
  for (int i = 1; i <= 15; ++i){
    BitBoardConstants::bitPosition[1 << i] =
        1 + BitBoardConstants::bitPosition[1 << (i - 1)];
  }
}

void BitBoardConstantInitializers::initializeBitCount() {
  BitBoardConstants::bitCount[0] = 0;
  for (int i = 1; i < (1 << 16); ++i) {
    BitBoardConstants::bitCount[i] =
        BitBoardConstants::bitCount[i >> 1] + (i & 1);
  }
}

void BitBoardConstantInitializers::initializeAll() {
  BitBoardConstantInitializers::initializeBitCount();
  BitBoardConstantInitializers::initializeBitPosition();
  BitBoardDatabase1::initialize();
}

BitBoardDatabase1::BitBoardDatabaseNode* BitBoardDatabase1::root;
BitBoardDatabase1::BitBoardDatabaseNode* BitBoardDatabase1::iAmHere;

BitBoardDatabase1::BitBoardDatabaseNode::BitBoardDatabaseNode(
    string roadFromParent)
      : roadFromParent(roadFromParent) { }

BitBoardDatabase1::BitBoardDatabaseNode::~BitBoardDatabaseNode() {
  for (vector<BitBoardDatabase1::BitBoardDatabaseNode*>::iterator it =
           nextMove.begin();
       it != nextMove.end();
       ++it) {
    delete *it;
  }
}

BitBoardDatabase1::BitBoardDatabaseNode*
BitBoardDatabase1::BitBoardDatabaseNode::addChild(string roadFromParent) {
  // Chech that the child is not already in the tree.
  for (vector<BitBoardDatabase1::BitBoardDatabaseNode*>::iterator it =
           this->nextMove.begin();
       it != this->nextMove.end();
       ++it) {
    if ((*it)->roadFromParent == roadFromParent) {
      return (*it);
    }
  }

  // Add it in.
  this->nextMove.push_back(
      new BitBoardDatabase1::BitBoardDatabaseNode(roadFromParent));
  return this->nextMove.back();
}

void BitBoardDatabase1::initialize() {
  BitBoardDatabase1::root =
      new BitBoardDatabase1::BitBoardDatabaseNode("START");
  BitBoardDatabase1::iAmHere = BitBoardDatabase1::root;

  // Load the databases from file.
  BitBoardDatabase1::loadFromFile("./database/a.txt");
  BitBoardDatabase1::loadFromFile("./database/b.txt");
  BitBoardDatabase1::loadFromFile("./database/c.txt");
  BitBoardDatabase1::loadFromFile("./database/d.txt");
  BitBoardDatabase1::loadFromFile("./database/e.txt");
}

void BitBoardDatabase1::destroy() {
  //delete BitBoardDatabase1::root;
  //BitBoardDatabase1::root = NULL;
  BitBoardDatabase1::iAmHere = NULL;
}

void BitBoardDatabase1::updateCurrentState(string thisMove) {
  // If we have falled outside of the opening moves database, updating ends.
  if (BitBoardDatabase1::iAmHere == NULL) {
    return;
  }

  vector<BitBoardDatabase1::BitBoardDatabaseNode*>::iterator it =
      BitBoardDatabase1::iAmHere->nextMove.begin();
  while (it != BitBoardDatabase1::iAmHere->nextMove.end()) {
    if (thisMove == (**it).roadFromParent) {
      break;
    }

    ++it;
  }

  if (it == BitBoardDatabase1::iAmHere->nextMove.end()) {
    BitBoardDatabase1::destroy();
    return;
  }

  BitBoardDatabase1::iAmHere = *it;
}

void BitBoardDatabase1::loadFromFile(const char* fileName) {
  FILE* f = fopen(fileName, "r");

  if (f == NULL) {
    printf("Error! Could not open flie '%s'.\n",fileName);
    return;
  }

  char buffer[301], *p;
  BitBoardDatabase1::BitBoardDatabaseNode* thisRoot; 
  char ignoreThese[] = " \t\n  ";
  ignoreThese[3] = 0x0d;
  ignoreThese[4] = 0x0a;
  while (fgets(buffer,300,f) != NULL) {
    if (buffer[0] == ' ' ||
        buffer[0] == '[' ||
        buffer[0] == '\n' ||
        buffer[0] == '\t' ||
        buffer[0] == 0 ||
        buffer[0] == 0x0d ||
        buffer[0] == 0x0a) {
      continue;
    }

    // Parse the line you've just read.
    thisRoot = BitBoardDatabase1::root;
    for (p = strtok(buffer, ignoreThese); p; p = strtok(NULL, ignoreThese)) {
      if (p[strlen(p) - 1] == '.') {
        continue;
      }

      thisRoot = thisRoot->addChild(string(p));
    }
  }

  fclose(f);
}

void BitBoardDatabase1::dump(BitBoardDatabase1::BitBoardDatabaseNode* root,
                             FILE *g,
                             int nrSpaces) {
  vector<BitBoardDatabase1::BitBoardDatabaseNode*>::iterator it;
  for (it = root->nextMove.begin(); it != root->nextMove.end(); ++it){
    for (int i = 1;i <= nrSpaces; ++i) {
      fprintf(g," ");
    }

    fprintf(g, "%s\n", (*it)->roadFromParent.c_str());
    BitBoardDatabase1::dump(*it, g, nrSpaces + 1);
  }
}

void BitBoardDatabase1::dumpToFile(const char* fileName) {
  FILE *g;
  if (strcmp(fileName,"stdout") == 0) {
    g = stdout;
  } else {
    g = fopen(fileName,"w");
  }
  BitBoardDatabase1::dump(BitBoardDatabase1::root, g, 0);
  fclose(g);
}

string BitBoardDatabase1::suggestNextMove() {
  // This is mostly a minimalist version.
  BitBoardDatabase1::BitBoardDatabaseNode* root = BitBoardDatabase1::iAmHere;
  if (root == NULL) {
    return "";
  }

  // The case in which the database no longer has any suggestions.
  if (root->nextMove.size() == 0) {
    BitBoardDatabase1::iAmHere = NULL;
    return "";
  }

  // Otherwise, just pick a variant to advance.
  return root->nextMove[rand() % (root->nextMove.size())]->roadFromParent;
}
