# include "BitBoard.h"
# include "BitBoardProtocol.h"
# include <string>
# include <iostream>
# include "BitBoardConstants.h"

using namespace std;

string buildMoveString(BitBoard& board) {

  // If castling has taken place, then one must return O-O or O-O-O.
  if (board.flags & 0x2000000000000000ull) {
    // Check what type of castling we're talking about. If bit 62 is 0, then
    // it is king-side castling.
    if ((board.flags & 0x4000000000000000ull) == 0) {
      return "move O-O\n";
    } else {
      return "move O-O-O\n";
    }
  }

  BitBoardPieceType pieceType =
      (BitBoardPieceType) ((board.flags & 0x000000000000001Cull) >> 2);
  BitBoardPlayerColor color =
      (BitBoardPlayerColor) ((board.flags >> 5) & 0x0000000000000001ull);
  // A move string is composed of:
  //    - "move "
  //    - <piece type>, if it is not a pawn
  //    - <piece disambiguation> (rank, file, if needed)
  //    - "x", if we are dealing with a capture
  //    - <piece destination>
  //    - "+", if we are dealing with a chess situation
  //A) Operation code.
  string sol = "move ";
  //B) Piece type.
  switch (pieceType){
    case BB_PION:
      if ( board.flags & 0x0000000000000040ull ) 
        sol +=  (char)((board.flags >> 8) & 0x000000000000000Full) + 'a' -1;
      break;
    case BB_CAL:
      sol += "N";
      break;
    case BB_TURN:
      sol += "R";
      break;
    case BB_NEBUN:
      sol += "B";
      break;
    case BB_REGINA:
      sol += "Q";
      break;
    case BB_REGE:
      sol += "K";
      break;
  }
  //C) Disambiguation.
  //  1. Look for all pieces which can reach that destination.
  if (pieceType != BB_PION && pieceType != BB_REGE) {
    int64 destination =
        BitBoardPositionHandlers::toMask(
            (BitBoardPosition)((board.flags & 0x0000000000FF0000ull) >> 16));
    int64 source =
        BitBoardPositionHandlers::toMask(
            (BitBoardPosition)((board.flags & 0x000000000000FF00ull) >> 8));
    int64 repository = 0;
    int64 candidates = board.view[color * 6 + pieceType];
    int64 current;

    // 2. Don't count the destination.
    board.view[color * 6 + pieceType] ^= destination;

    while (candidates) {
      current = RIGHTMOST_BIT(candidates);
      switch (pieceType) {
        case BB_CAL:
          if (BitBoardHandlers::threatenedByKnights(board,color,current) &
              destination) {
            repository |= current;
          }
          break;
        case BB_NEBUN:
          if (BitBoardHandlers::threatenedByBishops(board,color,current) &
              destination) {
            repository |= current;
          }
          break;
        case BB_TURN:
          if (BitBoardHandlers::threatenedByRooks(board,color,current) &
              destination) {
            repository |= current;
          }
          break;
        case BB_REGINA:
          if (BitBoardHandlers::threatenedByQueens(board,color,current) &
              destination) {
            repository |= current;
          }
          break;
      }
      candidates ^= current;
    }

    // Add it back in.
    board.view[color * 6 + pieceType] ^= destination;

    if (repository) {
      // If there is noting on the same file with the source, then sol gets the
      // file and it's over.
      BitBoardPosition destinationPosition =
          BitBoardPositionHandlers::fromMask(source);
      if ((wholecolumn[destinationPosition & 0x0F] & repository) == 0) {
        sol += (destinationPosition & 0x0F) + 'a' - 1;
      } else 
        if ((wholeline[(destinationPosition & 0xF0) >> 4] & repository) == 0) {
          sol += ((destinationPosition & 0xF0)>>4) + '0';
        } else {
          sol += (destinationPosition & 0x0F) + 'a' - 1;
          sol += ((destinationPosition & 0xF0)>>4) + '0';
        }
    }
  }
  //D) "x", if we are dealing with a capture.
  if (board.flags & 0x0000000000000040ull) {
    sol += "x";
  }
  //E) Piece destination.
  sol += BitBoardPositionHandlers::toString((BitBoardPosition)(
          (board.flags & 0x0000000000FF0000ull) >> 16 ));
  //F) Whether we are dealing with a promotion.
  if (pieceType == BB_PION && (board.flags & 0x1000000000000000ull)) {
    sol += '=';
    switch ( (board.flags >> 57) & 0x0000000000000007ull) {
      case BB_REGINA: 
        sol += 'Q';
        break;
      case BB_TURN:
        sol += 'R';
        break;
      case BB_CAL:
        sol += 'N';
        break;
      case BB_NEBUN:
        sol += 'B';
        break;
    }
  }
  //G) Whether we are in chess.
  if (board.flags & 0x0000000000000002ull) {
    sol +="+";
  }
  //H) End of line.
  sol += "\n";
  return sol;
}

// The function returns "2" if we are resigning.
int parseMove(string s, BitBoard& board, BitBoardPlayerColor opponent){
  // If we received a castling move, then it's very simple. Execute it.
  if (s == "O-O" || s == "O-O+") {
    BitBoardHandlers::castleKing(board, opponent, BB_KingSideCastling);
    return 1;
  } 
  if (s == "O-O-O" || s == "O-O-O+") {
    BitBoardHandlers::castleKing(board, opponent, BB_QueenSideCastling);
    return 1;
  }

  // If this is a chess mate, then it's over.
  if (s.find('#') != string::npos) {
    return 2;
  }

  // If there is nothing left but kings on the board, then it's a draw.
  int64 whitePieces =
      BitBoardHandlers::overlayedPieces(board,BB_WHITE) ^ board.view[BB_REGI_ALB];
  int64 blackPieces =
      BitBoardHandlers::overlayedPieces(board,BB_BLACK) ^ board.view[BB_REGI_NEGRU];
  if ((whitePieces | blackPieces) == 0) {
    return 3;
  }

  // The function checks whether the parameter is a move or not. It also
  // verifies the validity of the received moves.
  int64 destination;
  int64 repository, current;
  BitBoardPieceType pieceType;

  // PAWN.
  if (s[0] >= 'a' && s[0] <='h'){

    // Find the destination.
    if (s[1] == 'x'){
      destination =
          BitBoardPositionHandlers::packToMask(s[3] - '0', s[2] - 'a' + 1);
    } else { 
      destination =
          BitBoardPositionHandlers::packToMask(s[1] - '0', s[0] - 'a' + 1);
    }

    // Check whether the move was En Pasante. In other words, if s[1]=='x', but
    // the destination is free of any type of opponent pieces. Before moving
    // there, it's going to be free anyway. If so, then we'll have to remove the
    // dead pawn.
    if (opponent == BB_WHITE) {
      if (s[1] == 'x' && (blackPieces & destination) == 0) {
        // Forcefully execute the move and return because we know who can move
        // there, and there are no promotions allowed.
        board.view[BB_PIONI_NEGRU] ^= (destination >> 8);
        // Find out the pawn responsible for the move.
        // It is on the line of the destination, and on the column of s[0].
        current =
            BitBoardPositionHandlers::packToMask(
                ((BitBoardPositionHandlers::fromMask(destination >> 8)) >> 4),
                s[0] - 'a' + 1);
        BitBoardHandlers::forceMove(
            board, current, destination, opponent, BB_PION);
        return 1;
      }
    } else {
      if (s[1] == 'x' && (whitePieces & destination) == 0) {
        // Forcefully execute the move and return because we know who can move
        // there, and there no promotions possible.
        board.view[BB_PIONI_ALB] ^= (destination << 8);
        // We need to find the pawn who is responsible for the move.
        // It will be on the rank of the destination + 1, and the file of s[0].
        current = BitBoardPositionHandlers::packToMask(
            ((BitBoardPositionHandlers::fromMask(destination << 8)) >> 4),
            s[0] - 'a' + 1);
        BitBoardHandlers::forceMove(
            board, current, destination, opponent, BB_PION);
        return 1;
      }
    }

    // Look to the board, and find the pawn that can move there.
    // We should have exluded the pawns who cannot threaten the destination.

    int64 candidates = 0;
    repository = board.view[opponent * 6 + BB_PION];
    while (repository) {
      current = RIGHTMOST_BIT(repository);
      if ((BitBoardHandlers::availableToPawns(board, opponent, current) &
           destination)) {
        candidates |= current;
      }
      repository ^= current;
    }

    // Should we have to disambiguate, do that.
    if (s[1] == 'x') {
      current = candidates & wholecolumn[s[0] - 'a' + 1];
    } else {
      current = RIGHTMOST_BIT(candidates);
    }

    // It's also possible that the move is invalid.
    if (!current) {
# ifdef __STDOUTDUMP__
      cout << "( " << s << " ) " << "\tNot the syntax of a valid move!?\n";
# endif
      return 0;
    }

    // Execute the move.
    BitBoardHandlers::forceMove(
        board, current, destination, opponent, BB_PION);

    // Check whether the pawn was promoted to something else. We have different
    // situations for white and black.
    if ((opponent == BB_WHITE && (destination & 0xFF00000000000000ull)) ||
        (opponent == BB_BLACK && (destination & 0x00000000000000FFull))) {
      // We will have to check which of N, Q, B, and R are in the string.
      if (s.find('B') != string::npos) {
        BitBoardHandlers::upgradePawn(board, destination, opponent, BB_NEBUN);
      } else if (s.find('Q') != string::npos) {
        BitBoardHandlers::upgradePawn(board, destination, opponent, BB_REGINA);
      } else if (s.find('N') != string::npos) {
        BitBoardHandlers::upgradePawn(board, destination, opponent, BB_CAL);
      } else if (s.find('R') != string::npos) {
        BitBoardHandlers::upgradePawn(board, destination, opponent, BB_TURN);
      }
    }

    return 1;
  } else {
    // Any other piece.

    // Find out which piece has just moved.
    switch (s[0]) {
      case 'N': 
        pieceType = BB_CAL;
        break;
      case 'B':
        pieceType = BB_NEBUN;
        break;
      case 'R':
        pieceType = BB_TURN;
        break;
      case 'Q':
        pieceType = BB_REGINA;
        break;
      case 'K':
        pieceType = BB_REGE;
        break;
    }

    // Decide on supplementary information, if there is any.
    int length = s.length()-1;

    // If we're in chess, then the internal board will warn us.
    if (s[length] == '+') {
      length--;
    }

    // Build up the destination of the piece.
    length--;
    destination = BitBoardPositionHandlers::packToMask(s[length + 1] - '0',
                                                       s[length] - 'a' + 1);
    length--;

    // We are not interested in whether this is an attack. The interal board
    // will deal with this case separately.
    if (length != 0 && s[length] == 'x') {
      length--;
    }

    int64 obligatoryColumn = 0;
    int64 obligatoryLine = 0;
    // If we haven't reached the end, but we still have information.
    if (length != 0 && (s[length] < 'A' || s[length] > 'Z')) {
      if (s[length] <= 'h' && s[length] >= 'a') {
        obligatoryColumn = wholecolumn[s[length] - 'a' + 1];
      } else {
        obligatoryLine = wholeline[s[length] - '0'];
      }
      length--;
    }
    if (length != 0 && (s[length] < 'A' || s[length] > 'Z')) {
      if (s[length] <= 'h' && s[length] >= 'a') {
        obligatoryColumn = wholecolumn[s[length] - 'a' + 1];
      } else {
        obligatoryLine = wholeline[s[length] - '0'];
      }
      length--;
    }

    //Acum verificam care dintre piese poate sa mute
    repository = board.view[opponent * 6 + pieceType];
    if (obligatoryColumn) {
      repository &= obligatoryColumn;
    }
    if (obligatoryLine) {
      repository &= obligatoryLine;
    }

    while (repository) {
      current = RIGHTMOST_BIT(repository);
      if (pieceType == BB_CAL &&
          (BitBoardHandlers::threatenedByKnights(board, opponent, current) &
           destination)) {
        break;
      } else if (pieceType == BB_NEBUN &&
                 (BitBoardHandlers::threatenedByBishops(board, opponent, current) &
                  destination)) {
        break;
      } else if (pieceType == BB_TURN &&
                 (BitBoardHandlers::threatenedByRooks(board, opponent, current) &
                  destination)) {
        break;
      } else if (pieceType == BB_REGINA &&
                 (BitBoardHandlers::threatenedByQueens(board, opponent, current) &
                  destination)) {
        break;
      } else if (pieceType == BB_REGE &&
                 (BitBoardHandlers::threatenedByKings(board, opponent, current) &
                  destination)) {
        break;
      }
      repository ^= current;
    }

    // It's possible that the move is invalid.
    if (!repository) {
# ifdef __STDOUTDUMP__
      cout << "( " << s << " ) " << "\tNot the syntax of a valid move!?\n";
# endif
      return 0;
    }

    // Execute the move.
    BitBoardHandlers::forceMove(
        board, current, destination, opponent, pieceType);
    return 1;
  }
  return 0;
}

