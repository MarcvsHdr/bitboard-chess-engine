# include "BitBoard.h"
# include <stdio.h>
# include <string>

using namespace std;

BitBoardPosition BitBoardPositionHandlers::pack(int linie, int coloana) {
  BitBoardPosition sol = 0;
  sol = ((linie & 0x0F) << 4) | (coloana & 0x0F);
  return sol;
}

int64 BitBoardPositionHandlers::toMask(BitBoardPosition pos){
  // The formula is: 1<<(col - 1 + (line - 1) * 8)
  return ((int64)1) << ((pos & 0x0F) - 1 + 8 * ((pos >> 4) - 1));
}

BitBoardPosition BitBoardPositionHandlers::fromMask(int64 mask){
  BitBoardPosition sol = 0x10;
  while ((mask & 0xFFFFFFFFFFFFFF00ull) != 0) {
    sol += 0x10;
    mask >>= 8;
  }
  if (mask == 0x0000000000000001ull) return (sol | 0x01);
  if (mask == 0x0000000000000002ull) return (sol | 0x02);
  if (mask == 0x0000000000000004ull) return (sol | 0x03);
  if (mask == 0x0000000000000008ull) return (sol | 0x04);
  if (mask == 0x0000000000000010ull) return (sol | 0x05);
  if (mask == 0x0000000000000020ull) return (sol | 0x06);
  if (mask == 0x0000000000000040ull) return (sol | 0x07);
  if (mask == 0x0000000000000080ull) return (sol | 0x08);
# ifdef __STDOUTDEBUG__
  printf("Eroare in BitBoardPositionHandlers::fromMask(int64 mask)\n");
# endif
  return sol;
}

int64 BitBoardPositionHandlers::packToMask(int linie, int coloana){
  return ((int64)1) << ((linie - 1) * 8 + coloana - 1);
}

string BitBoardPositionHandlers::toString(BitBoardPosition pos){
  string sol = "";
  sol += 'a' + (pos & 0x0F) - 1;
  sol += '0' + (pos >> 4);
  return sol;
}

int BitBoardPositionHandlers::getManhattan(
    BitBoardPosition a, BitBoardPosition b) {
  return (GET_COLUMN(a) < GET_COLUMN(b) ?
          GET_COLUMN(b) - GET_COLUMN(a) :
          GET_COLUMN(a) - GET_COLUMN(b)) +
         (GET_LINE  (a) < GET_LINE  (b) ?
          GET_LINE  (b) - GET_LINE  (a) :
          GET_LINE  (a) - GET_LINE  (b));
}
