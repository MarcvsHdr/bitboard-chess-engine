# include "BitBoard.h"
# include "BitBoardThink.h"
# include <cstdlib>
# include <iostream>
# include <fstream>
# include <string>
# include "BitBoardProtocol.h"
# include "BitBoardConstants.h"
# include <algorithm>

# define INF 100000
# define _INF -100000
# define UNDEF -200000

# define MAX_DEPTH 5
// The first 4 moves are performed classicaly anyway.
# define FIRSTHORIZON 0
// If it makes it to 4 and it's quiet, jump straight to 1.
# define SECONDSTART 0
// Otherwise, continue on 2 in Quiet mode.
# define QUIESCENTSTART 0

# define ULTIMATE_HORIZON 0

# define HORIZON 0

using namespace std;

BitBoard nextStep[MAX_DEPTH + 1][MAX_NEXT_STEP + 1];
int nextStepValue[MAX_DEPTH + 1][MAX_NEXT_STEP + 1];
BitBoard searchStack[MAX_DEPTH + 1];
minimaxResult myProfit[MAX_DEPTH + 1];
minimaxResult hisProfit[MAX_DEPTH + 1];
minimaxResult hisBestProfit[MAX_DEPTH + 1];

string BitBoardThinkMethods::bitBoardThinkDatabaseRandom(
    BitBoard& board, BitBoardPlayerColor color) {
  // First check whether the database has a move to suggest.
  string databaseSuggestion = BitBoardDatabase1::suggestNextMove();
  // If so, just parse it and move it along.
  if (databaseSuggestion != "") {
# ifdef __STDOUTDUMP__
    cout << "Database-suggested move: " << databaseSuggestion << "\n";
# endif
    parseMove(databaseSuggestion, board, color);
    BitBoardDatabase1::updateCurrentState(databaseSuggestion);
    return buildMoveString(board);
  }
# ifdef __STDOUTDUMP__
  cout << "We're out of the scope of the moves in the database.\n";
# endif
  return BitBoardThinkMethods::bitBoardThink(board, color);
}

string BitBoardThinkMethods::bitBoardThinkRandom(BitBoard& board,
                                                 BitBoardPlayerColor color) {
  int nextStepLength;
  BitBoard* nextStep = new BitBoard[MAX_NEXT_STEP + 1];
  nextStepLength = 0;
  BitBoardHandlers::moveKnights(board, color, nextStep, nextStepLength);
  BitBoardHandlers::moveBishops(board, color, nextStep, nextStepLength);
  BitBoardHandlers::moveRooks  (board, color, nextStep, nextStepLength);
  BitBoardHandlers::moveQueens (board, color, nextStep, nextStepLength);
  BitBoardHandlers::movePawns  (board, color, nextStep, nextStepLength);
  BitBoardHandlers::moveKings  (board, color, nextStep, nextStepLength);
# ifdef __STDOUTDUMP__
  cout << "No. of possibilities: " << nextStepLength << "\n";
# endif
  if (nextStepLength == 0) {
    return "resign\n";
  }
  int choice = rand() % nextStepLength;
# ifdef __STDOUTDUMP__
  cout << "Solution no.: " << choice << "\n";
# endif
  board = nextStep[choice];
  delete[] nextStep;
  return buildMoveString(board);
}

string BitBoardThinkMethods::bitBoardThink(BitBoard& board,
                                           BitBoardPlayerColor color) {
  minimaxResult r, myBestSoFar;
  myBestSoFar.score = UNDEF;
  r = BitBoardThinkMethods::bitBoardAlfaBeta(
      MAX_DEPTH, board, color, myBestSoFar,
      BitBoardEvaluationHandlers::fancyGNUEvaluation);
  if (r.score == _INF) {
    return "resign\n";
  }
  board = r.move;
# ifdef __STDOUTDUMP__
  if (color == BB_WHITE) {
    cout << "I think I am the white player.\n";
  } else {
    cout << "I think I am the black player.\n";
  }
  BitBoardHandlers::dump(board, 0);
# endif
  return buildMoveString(r.move);
}

bool myComparator(const BitBoard& left, const BitBoard& right) {
  return *((short int*)((char*)(&right.flags) + 5)) <
      *((short int*)((char*)(&left.flags) + 5));
}

minimaxResult BitBoardThinkMethods::bitBoardQuiescent(
    int depth, BitBoard &board,
    BitBoardPlayerColor myColor,
    minimaxResult& myBestSoFar,
    BitBoardEvaluationHandlers::BitBoardEvaluationFunction evaluate) {
  int nextStepLength = 0, score1, score2, i;
  // If this is the end, evaluate winnings.
  if (depth == ULTIMATE_HORIZON) {
    evaluate(board, score1, score2);
    if (myColor == BB_WHITE) {
      myProfit[depth].score = score1 - score2;
    } else {
      myProfit[depth].score = score2 - score1;
    }
    myProfit[depth].move = board;
    return myProfit[depth];
  }

  // Consider winnings null and void.
  myProfit[depth].score = _INF;
  myProfit[depth].move = board;
  hisBestProfit[depth].score = UNDEF;
  // Expand the board on this level.
  BitBoardHandlers::moveKnights(board,myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveBishops(board,myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveRooks  (board,myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveQueens (board,myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::movePawns  (board,myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveKings  (board,myColor, nextStep[depth], nextStepLength);
  // Purge the board of puseless move.
  int trueNextStepLength = 0;
  for (i = 0; i < nextStepLength; ++i){
    if ((nextStep[depth][i].flags & 0x0000000000000040ull) ||
        (nextStep[depth][i].flags & 0x0000000000000002ull)) {
      if (trueNextStepLength < i) {
        nextStep[depth][trueNextStepLength++] = nextStep[depth][i];
      } else {
        trueNextStepLength++;
      }
    }
  }
  nextStepLength = trueNextStepLength;
  for (i = 0; i < nextStepLength; ++i) {
    searchStack[depth] = nextStep[depth][i];
    hisProfit[depth] = BitBoardThinkMethods::bitBoardQuiescent(
        depth - 1, nextStep[depth][i], (char)myColor ^ 1,
        hisBestProfit[depth], evaluate);
    if (hisBestProfit[depth].score == UNDEF ||
        hisProfit[depth].score < hisBestProfit[depth].score ||
        (hisProfit[depth].score == hisBestProfit[depth].score &&
         rand() % 2 == 0)) {
      hisBestProfit[depth].score = hisProfit[depth].score;
      myProfit[depth].score = -hisBestProfit[depth].score;
      myProfit[depth].move = searchStack[depth];
      if (myBestSoFar.score != UNDEF &&
          myBestSoFar.score < myProfit[depth].score){
        return myProfit[depth];
      }
    }
  }

  evaluate(board, score1, score2);
  if (myColor == BB_WHITE){
    myProfit[depth].score = score1 - score2;
  } else {
    myProfit[depth].score = score2 - score1;
  }
  myProfit[depth].move = board;
  return myProfit[depth];
}


minimaxResult BitBoardThinkMethods::bitBoardAlfaBeta(
    int depth,
    BitBoard &board,
    BitBoardPlayerColor myColor,
    minimaxResult& myBestSoFar,
    BitBoardEvaluationHandlers::BitBoardEvaluationFunction evaluate) {
  int nextStepLength = 0, score1, score2, i;
  // If this is the end, evaluate winnings.
  if (depth == ULTIMATE_HORIZON) {
    evaluate(board,score1,score2);
    if (myColor == BB_WHITE) {
      myProfit[depth].score = score1-score2;
    } else {
      myProfit[depth].score = score2-score1;
    }
    myProfit[depth].move = board;
    return myProfit[depth];
  }

  // Consider winnings null and void.
  myProfit[depth].score = _INF;
  myProfit[depth].move = board;
  hisBestProfit[depth].score = UNDEF;
  
  // Expand the board on this level.
  BitBoardHandlers::moveKnights(board, myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveBishops(board, myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveRooks  (board, myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveQueens (board, myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::movePawns  (board, myColor, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveKings  (board, myColor, nextStep[depth], nextStepLength);

  //sorting the board according to the value
  int scoreWhite, scoreBlack;
  for (i = 0; i < nextStepLength; ++i) {
    BitBoardEvaluationHandlers::materialEvaluation(
        nextStep[depth][i], scoreWhite, scoreBlack);
    if (myColor == BB_WHITE) {
      *((short int*)(((char*)(&nextStep[depth][i].flags)) + 5)) =
          (short int)(scoreWhite - scoreBlack);
    } else {
      *((short int*)(((char*)(&nextStep[depth][i].flags)) + 5)) =
          (short int)(scoreBlack - scoreWhite);
    }
  }
  sort(nextStep[depth], nextStep[depth] + nextStepLength, myComparator);
  
  for (i = 0; i < nextStepLength; ++i) {
    board = nextStep[depth][i];
    searchStack[depth] = nextStep[depth][i];
    hisProfit[depth] = BitBoardThinkMethods::bitBoardAlfaBeta(
        depth-1, nextStep[depth][i], (char)myColor ^ 1,
        hisBestProfit[depth], evaluate);
    if (hisBestProfit[depth].score == UNDEF ||
        hisProfit[depth].score < hisBestProfit[depth].score ||
        (hisProfit[depth].score == hisBestProfit[depth].score &&
         rand() % 2 == 0)) {
      hisBestProfit[depth].score = hisProfit[depth].score;
      myProfit[depth].score = -hisBestProfit[depth].score;
      myProfit[depth].move = searchStack[depth];
      if (myBestSoFar.score != UNDEF &&
          myBestSoFar.score < myProfit[depth].score){
        return myProfit[depth];
      }
    }
  }

  return myProfit[depth];
}


minimaxResult BitBoardThinkMethods::bitBoardMiniMax(
    int depth,
    BitBoard &board,
    BitBoardPlayerColor color,
    BitBoardEvaluationHandlers::BitBoardEvaluationFunction evaluate) {
  int nextStepLength = 0, score1, score2, i;
  BitBoardPlayerColor newcolor;
  minimaxResult r, r2;

  if (depth == HORIZON) {
    evaluate(board, score1, score2);
    if (color == BB_WHITE) {
      r.score = score1-score2;
      r.move = board;
      return r;
    } else {
      r.score = score2-score1;
      r.move = board;
      return r;
    }
  }

  int max = _INF;
  if (color == BB_WHITE) {
    newcolor = BB_BLACK;
  } else {
    newcolor = BB_WHITE;
  }
  BitBoardHandlers::moveKnights(board, color, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveBishops(board, color, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveRooks  (board, color, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveQueens (board, color, nextStep[depth], nextStepLength);
  BitBoardHandlers::movePawns  (board, color, nextStep[depth], nextStepLength);
  BitBoardHandlers::moveKings  (board, color, nextStep[depth], nextStepLength);
  if (nextStepLength == 0) {
    r.score = _INF;
    r.move = board;
    return r;
  }

  for (i = 0; i < nextStepLength; ++i) {
    board = nextStep[depth][i];
    r2 = BitBoardThinkMethods::bitBoardMiniMax(
        depth - 1, nextStep[depth][i], newcolor, evaluate);

    if (i == 0 || r2.score < max){
      max = r2.score;
      r.score = -r2.score;
      r.move = board;
    } else if (r2.score == max){
      int x = rand() % 2;
      if (x == 0) {
        r.score = -r2.score;
        r.move = board;
      }
      nextStep[depth][i] = board;
    }
  }

  return r;
}

