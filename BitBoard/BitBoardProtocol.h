# ifndef __BITBOARDPROTOCOL_H__
# define __BITBOARDPROTOCOL_H__

# include <string>
# include "BitBoard.h"

using namespace std;

string buildMoveString(BitBoard&);

int parseMove(string, BitBoard&, BitBoardPlayerColor);

# endif

