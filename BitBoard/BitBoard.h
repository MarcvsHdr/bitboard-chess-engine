# ifndef __BITBOARD_H__
# define __BITBOARD_H__

# include <string>

# define MAX_NEXT_STEP 100

using namespace std;

//BitBoard Constants

typedef unsigned long long int64;

# define RIGHTMOST_BIT(a) (((a) ^ ((a) - 1)) & (a))

# define COUNT_BITS(a) \
    (BitBoardConstants::bitCount[(a) & 0x0000000000FFFFull] + \
     BitBoardConstants::bitCount[((a) >> 16) & 0x0000000000FFFFull] + \
     BitBoardConstants::bitCount[((a) >> 32) & 0x0000000000FFFFull] + \
     BitBoardConstants::bitCount[((a) >> 48)])

# define POSITION_BIT(a) \
    (((a) & 0x00000000FFFFFFFFull) ? \
    (((a) & 0x000000000000FFFFull) ? \
     BitBoardConstants::bitPosition[(a) & 0x000000000000FFFFull] : \
     BitBoardConstants::bitPosition[((a) >> 16) & 0x000000000000FFFFull]): \
     (((a) & 0x0000FFFF00000000ull) ? \
      BitBoardConstants::bitPosition[((a) >> 32) & 0x000000000000FFFFull] : \
      BitBoardConstants::bitPosition[((a) >> 48) & 0x000000000000FFFFull]))

typedef unsigned char BitBoardPlayerColor;

const unsigned char BB_WHITE = 0;
const unsigned char BB_BLACK = 1;

typedef unsigned char BitBoardPieceType;

const unsigned char BB_PION = 0;
const unsigned char BB_TURN = 1;
const unsigned char BB_CAL = 2;
const unsigned char BB_NEBUN = 3;
const unsigned char BB_REGINA = 4;
const unsigned char BB_REGE = 5;

typedef unsigned char BitBoardSpecificPieceType;

const unsigned char BB_PIONI_ALB = 0;
const unsigned char BB_TURNURI_ALB = 1;
const unsigned char BB_CAI_ALB = 2;
const unsigned char BB_NEBUNI_ALB = 3;
const unsigned char BB_REGINE_ALB = 4;
const unsigned char BB_REGI_ALB = 5;
const unsigned char BB_PIONI_NEGRU = 6;
const unsigned char BB_TURNURI_NEGRU = 7;
const unsigned char BB_CAI_NEGRU = 8;
const unsigned char BB_NEBUNI_NEGRU = 9;
const unsigned char BB_REGINE_NEGRU = 10;
const unsigned char BB_REGI_NEGRU = 11;

typedef unsigned char BitBoardPlayerGap;

const unsigned char BB_PlayerGap = 6;

typedef unsigned char BitBoardCastlingType;

const unsigned char BB_KingSideCastling = 0;
const unsigned char BB_QueenSideCastling = 1;

// The BitBoard representations are as follows
// slice[0] => white pawns
// slice[1] => white towers
// slice[2] => white knights
// slice[3] => white bishops
// slice[4] => white queens
// slice[5] => white king
//
// slice[0+6] => black pawns
// slice[1+6] => black towers
// slice[2+6] => black knights
// slice[3+6] => black bishops
// slice[4+6] => black queens
// slice[5+6] => black king

typedef struct BitBoard {
  int64 view[12];
  int64 flags;
  char flagsCastling;
} BitBoard;

//   The encoding of the board squares is done as follows:
//
//      b56 b57 b58 b59 b60 b61 b62 b63 -> bit rank increases
//
//      ... ... ... ... ... ... ... ... 
//
//      b8  b9  b10 b11 b12 b13 b14 b15 
//
//   -> b0  b1  b2  b3  b4  b5  b6  b7  
//
//
//   The encoding of the falgs pieces is done as follows:
//
//                                       -> bit rank increases
//
//     b56 b57 b58 b59 b60 b61 b62 b63   | These corner fields specify whether
//                                       | the black king may castle there.
//     b48 b49 b50 b51 b52 b53 b54 b55
//
//     b40 b41 b42 b43 b44 b45 b46 b47
//
//     b32 b33 b34 b35 b36 b37 b38 b39   | These two fields mean that a pawn
//                                       | occupying them can be killed En Pasante
//     b24 b25 b26 b27 b28 b29 b30 b31   | by another pawn.
//
//     b16 b17 b18 b19 b20 b21 b22 b23
//
//     b8  b9  b10 b11 b12 b13 b14 b15 
//
//  -> b0  b1  b2  b3  b4  b5  b6  b7    | These corner fields specify whether
//                                       | the white king may castle there.

typedef unsigned char BitBoardPosition;

# define GET_COLUMN(a) ((a) & 0x0F)
# define GET_LINE(a) (((a) & 0xF0) >> 4)
# define GET_RING(a) \
    ((((a) << 1) & 0xFEFEFEFEFEFEFEFEull) | \
     (((a) >> 1) & 0x7F7F7F7F7F7F7F7Full) | \
     (((a) << 8)) | \
     (((a) >> 8)) | \
     (((a) << 7) & 0x7F7F7F7F7F7F7F7Full) | \
     (((a) << 9) & 0xFEFEFEFEFEFEFEFEull) | \
     (((a) >> 7) & 0xFEFEFEFEFEFEFEFEull) | \
     (((a) >> 9) & 0x7F7F7F7F7F7F7F7Full) )

//  The encoding of the positions within a char for BitBoardPosition is done
//  as follows:
//
//  -> c0  c1  c2  c3  l0  l1  l2  l3 -> bit rank increases

class BitBoardHandlers{

 public:
  static void reset(BitBoard&);

  // Debugging functions.
  static void dump(BitBoard&, int);
  static void dumpINT64(int64);

  // Forcing a move.
  static void forceMove(BitBoard&, BitBoardPosition, BitBoardPosition);
  static void forceMove(BitBoard&, BitBoardPosition, BitBoardPosition,
                        BitBoardPlayerColor, BitBoardPieceType);
  static void forceMove(BitBoard&, int64, int64);
  static void forceMove(BitBoard&, int64, int64, BitBoardPlayerColor,
                        BitBoardPieceType);
  static int64 overlayedPieces(BitBoard&, BitBoardPlayerColor);
  // Expanding the board, for each type of piece.
  static void moveKnights(BitBoard&, BitBoardPlayerColor, BitBoard*, int&);
  static void moveBishops(BitBoard&, BitBoardPlayerColor, BitBoard*, int&);
  static void moveRooks  (BitBoard&, BitBoardPlayerColor, BitBoard*, int&);
  static void moveQueens (BitBoard&, BitBoardPlayerColor, BitBoard*, int&);
  static void movePawns  (BitBoard&, BitBoardPlayerColor, BitBoard*, int&);
  static void moveKings  (BitBoard&, BitBoardPlayerColor, BitBoard*, int&);
  // Determining whether a certain square is threatened by a player's pieces,
  // for each piece.
  static int64 threatenedByKnights(BitBoard&, BitBoardPlayerColor, int64);
  static int64 threatenedByBishops(BitBoard&, BitBoardPlayerColor, int64);
  static int64 threatenedByRooks  (BitBoard&, BitBoardPlayerColor, int64);
  static int64 threatenedByQueens (BitBoard&, BitBoardPlayerColor, int64);
  static int64 threatenedByPawns  (BitBoard&, BitBoardPlayerColor, int64);
  static int64 threatenedByKings  (BitBoard&, BitBoardPlayerColor, int64);
  // Disambiguating for pieces which have special moves.
  static int64 availableToPawns   (BitBoard&, BitBoardPlayerColor, int64);
  // Unusual actions.
  static void upgradePawn(BitBoard&, int64,
                          BitBoardPlayerColor, BitBoardPieceType);
  static void castleKing (BitBoard&,
                          BitBoardPlayerColor, BitBoardCastlingType);
  static void enPassante (BitBoard&, int64, int64,
                          BitBoardPlayerColor, BitBoardPieceType);
  // Determining chess situations.
  static bool isThreatenedByHim (BitBoard&, BitBoardPlayerColor, int64);
  static bool isInChess (BitBoard&, BitBoardPlayerColor);
  // Determining how protected a piece is.
  static bool isProtected (BitBoard&, BitBoardPlayerColor, int64);
  static int  protectionIndex (BitBoard&, BitBoardPlayerColor, int64);
  static int  protectionValue (BitBoard&, BitBoardPlayerColor, int64);
  static int  threatIndex (BitBoard&, BitBoardPlayerColor, int64);
  static int  threatValue (BitBoard&, BitBoardPlayerColor, int64);
};

class BitBoardPositionHandlers{

 public:
  static BitBoardPosition pack(int, int);
  static BitBoardPosition fromMask(int64);
  static int64 toMask(BitBoardPosition);
  static int64 packToMask(int, int);
  static string toString(BitBoardPosition);
  static int getManhattan(BitBoardPosition, BitBoardPosition);
};

# endif
