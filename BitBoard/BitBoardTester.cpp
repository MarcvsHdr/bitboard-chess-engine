# ifdef __BITBOARDTESTER__

# include "BitBoard.h"
# include <stdio.h>
# include <stdlib.h>
# include <time.h>

int main()
{
  srand(time(NULL));
  BitBoard bitBoard;
  BitBoardHandlers::reset(bitBoard);
  // Take out a move from the white knights, and execute one of them.
  BitBoardPlayerColor me = BB_WHITE;
  BitBoard nextStep[100];
  unsigned char nextStepLength;
  int firsttime = 1;
  do {
    BitBoardHandlers::dump(bitBoard, firsttime ^ 1);
    firsttime = 0;
    nextStepLength = 0;
    BitBoardHandlers::moveKnights(bitBoard, me, nextStep, nextStepLength);
    BitBoardHandlers::moveBishops(bitBoard, me, nextStep, nextStepLength);
    BitBoardHandlers::moveRooks  (bitBoard, me, nextStep, nextStepLength);
    BitBoardHandlers::moveQueens (bitBoard, me, nextStep, nextStepLength);
    BitBoardHandlers::movePawns  (bitBoard, me, nextStep, nextStepLength);
    BitBoardHandlers::moveKings  (bitBoard, me, nextStep, nextStepLength);
    if (nextStepLength == 0) {
      break;
    }
    bitBoard = nextStep[rand() % nextStepLength];
    me ^= 1;
    fflush(stdin);
    getchar();
  } while (1);
  return 0;
}
# endif
