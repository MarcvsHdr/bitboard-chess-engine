# include "BitBoard.h"
# include "BitBoardThink.h"
# include <cstdlib>
# include <iostream>
# include <fstream>
# include <string>
# include "BitBoardProtocol.h"
# include "BitBoardConstants.h"

int BitBoardEvaluationHandlers::standardValueSystem = 3;

const int BitBoardEvaluationHandlers::standardValue[15][5] = {
  {100, 500, 310, 330,  790},
  {100, 548, 305, 350,  994},
  {100, 570, 350, 350, 1003},
  {100, 500, 300, 300,  950},
  {100, 550, 350, 350, 1000},
  {100, 500, 350, 350,  850},
  {100, 500, 300, 301,  900},
  {100, 500, 350, 351, 1000},
  {100, 500, 300, 325,  900},
  {100, 500, 325, 325,  975},
  {100, 510, 320, 333,  880},
  {100, 500, 350, 350,  950},
  {100, 450, 300, 300,  900},
  {100, 640, 240, 400, 1040},
  {100, 520, 330, 330,  980} };

void BitBoardEvaluationHandlers::setStandardValueSystem(int systemNumber) {
  if (systemNumber < BitBoardEvaluationHandlers::standardValueSystemCount) {
    BitBoardEvaluationHandlers::standardValueSystem = systemNumber;
  }
}

void BitBoardEvaluationHandlers::materialEvaluation(BitBoard& board,
                                                    int& whiteValue,
                                                    int& blackValue){
  whiteValue = 0;
  blackValue = 0;
  for (int i = BB_PIONI_ALB; i <= BB_REGINE_ALB; i++){
    whiteValue += COUNT_BITS(board.view[i]) *
        BitBoardEvaluationHandlers::standardValue[
            BitBoardEvaluationHandlers::standardValueSystem][i];
  }
  for (int i = BB_PIONI_NEGRU; i<= BB_REGINE_NEGRU; i++){
    blackValue += COUNT_BITS(board.view[i]) *
        BitBoardEvaluationHandlers::standardValue[
            BitBoardEvaluationHandlers::standardValueSystem][i - 6];
  }
}

void BitBoardEvaluationHandlers::fancyGNUEvaluation(BitBoard& board,
                                                    int& whiteValue,
                                                    int& blackValue) {
  // Materially evaluate left and right.
  BitBoardEvaluationHandlers::setStandardValueSystem(14); 
  BitBoardEvaluationHandlers::materialEvaluation(board, whiteValue, blackValue);
  // Add in positional bonuses for all pieces, except for the pawns.
  whiteValue += Bonuses::allPiecesPositionBonus(board, BB_WHITE);
  blackValue += Bonuses::allPiecesPositionBonus(board, BB_BLACK);
  // Pawns only have positoinal bonuses (including fear penalties).
  int64 repository;
  for (repository = board.view[BB_PION];
       repository;
       repository ^= RIGHTMOST_BIT(repository)){
    whiteValue += Bonuses::positionBonus(board, RIGHTMOST_BIT(repository),
                                         BB_PION, BB_WHITE);
  }

  for (repository = board.view[BB_PION+BB_PlayerGap];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    blackValue += Bonuses::positionBonus(board, RIGHTMOST_BIT(repository),
                                         BB_PION, BB_BLACK);
  }

  // The tower has mobility, positional, and fear bonuses.
  for (repository = board.view[BB_TURN];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    whiteValue += Bonuses::mobilityBonus(board, RIGHTMOST_BIT(repository),
                                         BB_TURN, BB_WHITE);
    whiteValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_TURN, BB_WHITE);
  }
  for (repository = board.view[BB_TURN + BB_PlayerGap];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    blackValue += Bonuses::mobilityBonus(board, RIGHTMOST_BIT(repository),
                                         BB_TURN, BB_BLACK);
    blackValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_TURN, BB_BLACK);
  }

  // The knight has fear bonuses.
  for (repository = board.view[BB_CAL];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    whiteValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_CAL, BB_WHITE);
  }
  for (repository = board.view[BB_CAL+BB_PlayerGap];
       repository;
       repository ^= RIGHTMOST_BIT(repository)){
    blackValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_CAL, BB_BLACK);
  }

  // Bishops have all the bonuses.
  for (repository = board.view[BB_NEBUN];
       repository;
       repository ^= RIGHTMOST_BIT(repository)) {
    whiteValue += Bonuses::mobilityBonus(board, RIGHTMOST_BIT(repository),
                                         BB_NEBUN, BB_WHITE);
    whiteValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_NEBUN, BB_WHITE);
    whiteValue += Bonuses::specialBonus(board, RIGHTMOST_BIT(repository),
                                        BB_NEBUN, BB_WHITE);
  }

  for (repository = board.view[BB_NEBUN+BB_PlayerGap];
       repository;
       repository ^= RIGHTMOST_BIT(repository)){
    blackValue += Bonuses::mobilityBonus(board, RIGHTMOST_BIT(repository),
                                         BB_NEBUN, BB_BLACK);
    blackValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_NEBUN, BB_BLACK);
    blackValue += Bonuses::specialBonus(board, RIGHTMOST_BIT(repository),
                                        BB_NEBUN, BB_BLACK);
  }

  // The queen has fear bonuses.
  for (repository = board.view[BB_REGINA];
       repository;
       repository ^= RIGHTMOST_BIT(repository)){
    whiteValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_REGINA, BB_WHITE);
  }
  for (repository = board.view[BB_REGINA+BB_PlayerGap];
       repository;
       repository ^= RIGHTMOST_BIT(repository)){
    blackValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_REGINA, BB_BLACK);
  }

  // The king has fear and special bonuses.
  for (repository = board.view[BB_REGE];
       repository;
       repository ^= RIGHTMOST_BIT(repository)){
    whiteValue += Bonuses::    fearBonus(board, RIGHTMOST_BIT(repository),
                                         BB_REGE, BB_WHITE);
    whiteValue += Bonuses:: specialBonus(board, RIGHTMOST_BIT(repository),
                                         BB_REGE, BB_WHITE);
  }

  for (repository = board.view[BB_REGE+BB_PlayerGap];
       repository;
       repository ^= RIGHTMOST_BIT(repository)){
    blackValue += Bonuses::fearBonus(board, RIGHTMOST_BIT(repository),
                                     BB_REGE, BB_BLACK);
    blackValue += Bonuses::specialBonus(board, RIGHTMOST_BIT(repository),
                                        BB_REGE, BB_BLACK);
  }
}

int BitBoardEvaluationHandlers::Bonuses::mobilityBonus(
    BitBoard& board,
    int64 positionMask,
    BitBoardPieceType type,
    BitBoardPlayerColor myColor) {
  int totalBonus = 0;
  BitBoardPosition myPiecePosition =
      BitBoardPositionHandlers::fromMask(positionMask);

  switch (type) {
    case BB_TURN:
      // +2 points for each position in which it can move.
      totalBonus += COUNT_BITS(BitBoardHandlers::threatenedByRooks(
              board, myColor, positionMask)) * 2;
      // +10 points if no own pawns are on the same file.
      if ((board.view[BB_PION+BB_PlayerGap * myColor] &
           wholecolumn[GET_COLUMN(myPiecePosition)]) == 0) {
        totalBonus += 10;
        // +4 points if there are none of the opponent's pawns on the same file.
        if ((board.view[BB_PION+BB_PlayerGap * (myColor ^ 1)] &
             wholecolumn[GET_COLUMN(myPiecePosition)]) == 0 ){
          totalBonus += 4;
        }
      }
      break;
    case BB_NEBUN:
      // No X-RAY vision.
      totalBonus += COUNT_BITS(BitBoardHandlers::threatenedByBishops(
              board, myColor, positionMask)) * 2 - 4;
      break;
  }
  return totalBonus;
}

const int BitBoardEvaluationHandlers::Bonuses::knightPositionalBonus[9][9] = {
  { 0, 0, 0, 0, 0, 0, 0, 0, 0},
  { 0, 0, 5,10,15,15,10, 5, 0},
  { 0, 5,10,15,20,20,15,10, 5},
  { 0,10,15,20,25,25,20,15,10},
  { 0,15,20,25,30,30,25,20,15},
  { 0,15,20,25,30,30,25,20,15},
  { 0,10,15,20,25,25,20,15,10},
  { 0, 5,10,15,20,20,15,10, 5},
  { 0, 0, 5,10,15,15,10, 5, 0}
};

const int BitBoardEvaluationHandlers::Bonuses::bishopPositionalBonus[9][9] = {
  { 0, 0, 0, 0, 0, 0, 0, 0, 0},
  { 0,14,14,14,14,14,14,14,14},
  { 0,14,16,16,16,16,16,16,14},
  { 0,14,16,19,19,19,19,16,14},
  { 0,14,16,19,22,22,19,16,14},
  { 0,14,16,19,22,22,19,16,14},
  { 0,14,16,19,19,19,19,16,14},
  { 0,14,16,16,16,16,16,16,14},
  { 0,14,14,14,14,14,14,14,14}
};

const int BitBoardEvaluationHandlers::Bonuses::pawnPositionalBonus[9] = {
  0, 12, 14, 16, 20, 20, 16, 14, 12
};

const int BitBoardEvaluationHandlers::Bonuses::pawnAdvancementBonus[9] = {
  0, 1, 2, 3, 4, 4, 3, 2, 1
};

int BitBoardEvaluationHandlers::Bonuses::positionBonus(
    BitBoard& board,
    int64 positionMask,
    BitBoardPieceType type,
    BitBoardPlayerColor myColor) {
  int totalBonus = 0;
  BitBoardPosition myPiecePosition =
      BitBoardPositionHandlers::fromMask(positionMask);
  BitBoardPosition enemyKingPosition =
      BitBoardPositionHandlers::fromMask(
          board.view[BB_REGE + BB_PlayerGap * ((char)myColor ^ 1)]);
  BitBoardPosition myKingPosition =
      BitBoardPositionHandlers::fromMask(
          board.view[BB_REGE + BB_PlayerGap * myColor]);
  int manhattanDistance =
      BitBoardPositionHandlers::getManhattan(myPiecePosition, myKingPosition);
  bool isolated = true;
  char myPieceColumn = GET_COLUMN(myPiecePosition);
  char myPieceLine = GET_LINE(myPiecePosition);
  int64 myPawns = board.view[BB_PION + myColor * BB_PlayerGap];
  int64 hisPawns = board.view[BB_PION + (1 - myColor) * BB_PlayerGap];
  int64 myPieces = BitBoardHandlers::overlayedPieces(board, myColor);
  int64 hisPieces = BitBoardHandlers::overlayedPieces(board, (char)myColor ^ 1);
  int64 allPieces = myPieces | hisPieces;
  char scale = 1;
  switch (type) {
    case BB_PION:
      // Isolated pawns are penalized.
      if (myPieceColumn > 1 && (myPawns & wholecolumn[myPieceColumn - 1])) {
        isolated = false;
      }
      if (isolated && myPieceColumn < 8 &&
          (myPawns & wholecolumn[myPieceColumn + 1])) {
        isolated = false;
      }
      if (isolated) {
        totalBonus -= BitBoardEvaluationHandlers::Bonuses::
            pawnPositionalBonus[myPieceColumn];
      } else {
        if (COUNT_BITS(myPawns & wholecolumn[myPieceColumn]) > 1) {
          totalBonus -= 12;
        }
      }

      // Pawns on the second line, and files "d" and "e" get -10, plus an extra
      // -15 if they become stuck.
      if (positionMask & 0x0018000000001800ull) {
        totalBonus -= 10;
        if ((BitBoardHandlers::threatenedByPawns(board, myColor, positionMask) &
             hisPieces) == 0) {
          if ((myColor == BB_WHITE && ((positionMask << 8) & allPieces) == 0) ||
              (myColor == BB_BLACK && ((positionMask >> 8) & allPieces) == 0)) {
            totalBonus -= 15;
          }
        }
      }

      // Being within 2 squared of the king gets you +10, but only towards the
      // end of the game.
      allPieces ^= (myPawns | hisPawns);
      if ((COUNT_BITS(allPieces)) <= 5)
        if (manhattanDistance <= 2) {
          totalBonus += 10;
        }
      // Backward pawns get -6.
      if ((BitBoardHandlers::threatenedByPawns(board, myColor, 0) &
           (myColor == BB_WHITE ?
            (positionMask | (positionMask <<8 )) :
            (positionMask | (positionMask >> 8)))) == 0 ) {
        totalBonus -= 6;
      }

      // Pawns get a bonus based on their rank. We are staking on pawn promotion
      // if the opponent has at most X pieces besides their king.
      if (COUNT_BITS(hisPieces ^ enemyKingPosition ^ hisPawns) <= 2){
        scale = 10;
      }
      totalBonus += (myPieceLine - 2) *
          BitBoardEvaluationHandlers::Bonuses::pawnAdvancementBonus[
            myPieceColumn] * scale;
      break;
    case BB_TURN:
      // Penalize the tower by -5 if it should be too close to the king.
      if (manhattanDistance <= 2){
        totalBonus -= manhattanDistance * 5;
      }
      break;
    case BB_NEBUN:
      // Bishops gets a bonus based on how close they are to the middle of the
      // board.
      totalBonus += BitBoardEvaluationHandlers::Bonuses::
          bishopPositionalBonus[myPieceLine][myPieceColumn];
      // Penalize the bishop by -5 if it is too close to the king, or it has an
      // even manhattan distance to it.
      if (((manhattanDistance & 1) || (manhattanDistance == 2)) &&
          (manhattanDistance <= 4) ) {
        totalBonus -= manhattanDistance * 5;
      }
      break;
    case BB_CAL:
      // Knights also get a bonus based on how close they are to the centre of
      // the board.
      totalBonus += BitBoardEvaluationHandlers::Bonuses::
          knightPositionalBonus[myPieceLine][myPieceColumn];
      // Penalize the knight by -1 if it is too close to the enemy king.
      totalBonus -= manhattanDistance;
      break;
    case BB_REGINA:
      // Penalize the queen by -3 if it is too close to the enemy king.
      totalBonus -= manhattanDistance * 3;
      break;
    case BB_REGE:
      // -8 points if there are no pawns nearby.
      if ((GET_RING(positionMask) & myPawns) == 0) {
        totalBonus -= 8;
      }

      // Penalize pieces on the first line by -3.
      if (myColor == BB_WHITE) {
        myPieces = (myPieces ^ myPawns) & 0x0000000000000099ull;
      } else {
        myPieces = (myPieces ^ myPawns) & 0x9900000000000000ull;
      }
      totalBonus -= (COUNT_BITS(myPieces)) * 3;
      break;
  }

  return totalBonus;
}

int BitBoardEvaluationHandlers::Bonuses::fearBonus(
    BitBoard& board,
    int64 positionMask,
    BitBoardPieceType type,
    BitBoardPlayerColor myColor){
  int totalBonus = 0;
  bool getsBonus;
  switch (type){
    case BB_CAL:
      // If a knight cannot be attacked by an enemy pawn, +8 bonus.
      getsBonus = true;
      for (int64 repository =
             board.view[BB_PION + ((char)myColor ^ 1) * BB_PlayerGap];
           repository;
           repository ^= RIGHTMOST_BIT(repository)) {
        if (BitBoardHandlers::threatenedByPawns(board,
                                                (char)myColor ^ 1,
                                                positionMask)) {
          getsBonus = false;
          break;
        }
      }

      if (getsBonus) {
        totalBonus += 8;
      }
      break;
  }

  // Pieces which are threatened and undefended, or which are being threatened
  // by a force larger than they can adjust, get a 10p penalty.
  if (BitBoardHandlers::protectionIndex(board, myColor, positionMask) == 0 &&
      BitBoardHandlers::threatIndex(board, myColor, positionMask)) {
    totalBonus -= 10;
  } else {
    if (BitBoardHandlers::protectionValue(board, myColor, positionMask) +
        BitBoardEvaluationHandlers::
          standardValue[BitBoardEvaluationHandlers::standardValueSystem][type] >
        BitBoardHandlers::threatValue(board, myColor, positionMask)) {
      totalBonus -= 10;
    }
  }
  return totalBonus;
}

int BitBoardEvaluationHandlers::Bonuses::specialBonus(
    BitBoard& board,
    int64 positionMask,
    BitBoardPieceType type,
    BitBoardPlayerColor myColor) {
  int totalBonus = 0;
  int64 enemyKingPositionMask =
      board.view[BB_REGE + BB_PlayerGap * (myColor ^ 1)];
  switch (type) {
    case BB_NEBUN:
      // The bishop gets +1 for each dead piece.
      totalBonus += 32 -
          (COUNT_BITS(BitBoardHandlers::overlayedPieces(board, BB_WHITE)) +
           COUNT_BITS(BitBoardHandlers::overlayedPieces(board, BB_BLACK)));
      // Likewise, the bishops gets +5 if it is adjacent to the enemy king.
      enemyKingPositionMask = GET_RING(enemyKingPositionMask);
      if (BitBoardHandlers::threatenedByRooks(board, myColor, positionMask) &
          enemyKingPositionMask) {
        totalBonus += 5;
      }
      break;
    case BB_REGE:
      // If the king has castled, then it gets +10p.
      if (board.flags & 0x2000000000000000ull) {
        totalBonus += 50;
      }
      // Alternatively, the king may have moved, but not castled.
      if (((board.flags & 0x0000000000000016ull) >> 2) == BB_REGE) {
        if ((myColor == BB_WHITE && (board.flagsCastling & 0x01)) ||
            (myColor == BB_BLACK && (board.flagsCastling & 0x02))) {
          totalBonus -= 40;
        }
      }
      // If you're in chess, you acquire anore +20p penalty.
      if (board.flags & 0x0000000000000002ull) {
        totalBonus -= 20;
      }
      break;
  }
  return totalBonus;
}

int BitBoardEvaluationHandlers::Bonuses::allPiecesPositionBonus(
    BitBoard& board,
    BitBoardPlayerColor myColor) {
  int totalBonus = 0;
  int64 enemyKing = board.view[BB_REGE + ((char)myColor ^ 1) * BB_PlayerGap];
  int64 rightmostBit;
  short int difference;

  // Knights.
  int64 thePieces = board.view[BB_CAL + myColor * BB_PlayerGap];
  totalBonus +=
      (COUNT_BITS(thePieces & 0x4281000000008142ull)) * 5 +
      (COUNT_BITS(thePieces & 0x2442810000824224ull)) * 10 +
      (COUNT_BITS(thePieces & 0x1824428181422418ull)) * 15 +
      (COUNT_BITS(thePieces & 0x0018244242241800ull)) * 20 +
      (COUNT_BITS(thePieces & 0x0000182424180000ull)) * 25 +
      (COUNT_BITS(thePieces & 0x0000001818000000ull)) * 30;

  while (thePieces) {
    rightmostBit = RIGHTMOST_BIT(thePieces);
    difference = rightmostBit < enemyKing ?
        (POSITION_BIT(enemyKing) - POSITION_BIT(rightmostBit)) :
        (POSITION_BIT(rightmostBit) - POSITION_BIT(enemyKing));
    totalBonus -= (difference % 8) + (difference / 8);
    thePieces ^= rightmostBit;
  }

  // Bishops.
  thePieces = board.view[BB_NEBUN + myColor * BB_PlayerGap];
  totalBonus +=
      (COUNT_BITS(thePieces & 0xFF818181818181FFull)) * 14 +
      (COUNT_BITS(thePieces & 0x007E424242427E00ull)) * 16 +
      (COUNT_BITS(thePieces & 0x00003C24243C0000ull)) * 19 +
      (COUNT_BITS(thePieces & 0x0000001818000000ull)) * 22;

  while (thePieces) {
    rightmostBit = RIGHTMOST_BIT(thePieces);
    difference = rightmostBit < enemyKing ?
        (POSITION_BIT(enemyKing) - POSITION_BIT(rightmostBit)) :
        (POSITION_BIT(rightmostBit) - POSITION_BIT(enemyKing));
    totalBonus -= (((difference % 8) + (difference / 8)) & 0x00000003) * 5;
    thePieces ^= rightmostBit;
  }

  // Towers.
  thePieces = board.view[BB_TURN + myColor * BB_PlayerGap];
  while (thePieces) {
    rightmostBit = RIGHTMOST_BIT(thePieces);
    difference = rightmostBit<enemyKing ?
        (POSITION_BIT(enemyKing) - POSITION_BIT(rightmostBit)) :
        (POSITION_BIT(rightmostBit) - POSITION_BIT(enemyKing));
    totalBonus -= (((((difference % 8) + (difference / 8)) - 1) & 0x00000001) + 1) * 5;
    thePieces ^= rightmostBit;
  }

  // Queens.
  thePieces = board.view[BB_REGINA + myColor * BB_PlayerGap];
  while (thePieces) {
    rightmostBit = RIGHTMOST_BIT(thePieces);
    difference = rightmostBit<enemyKing ?
        (POSITION_BIT(enemyKing) -POSITION_BIT(rightmostBit)) :
        (POSITION_BIT(rightmostBit) - POSITION_BIT(enemyKing));
    totalBonus -= ((difference % 8) + (difference / 8)) * 3;
    thePieces ^= rightmostBit;
  }

  // King.
  if ((GET_RING(board.view[BB_REGE + myColor * BB_PlayerGap]) &
       board.view[BB_PION + myColor * BB_PlayerGap]) == 0) {
    totalBonus -= 8;
  }

  // Specials.
  thePieces = (BitBoardHandlers::overlayedPieces(board,myColor) ^
               board.view[BB_PION + myColor * BB_PlayerGap]) &
      (0x0000000000000066ull << (myColor * 7));
  totalBonus -= (COUNT_BITS(thePieces)) * 3;
  return totalBonus;
}

int BitBoardEvaluationHandlers::Bonuses::allKnightsPositionBonus(
    BitBoard& board,
    BitBoardPlayerColor myColor){
  // Knights get the following bonuses.
  int64 theKnights = board.view[BB_CAL + myColor * BB_PlayerGap];
  int64 enemyKing = board.view[BB_REGE + ((char)myColor ^ 1) * BB_PlayerGap];
  int64 rightmostBit;
  short int difference;
  int totalBonus =
      (COUNT_BITS(theKnights & 0x4281000000008142ull)) * 5 +
      (COUNT_BITS(theKnights & 0x2442810000824224ull)) * 10 +
      (COUNT_BITS(theKnights & 0x1824428181422418ull)) * 15 +
      (COUNT_BITS(theKnights & 0x0018244242241800ull)) * 20 +
      (COUNT_BITS(theKnights & 0x0000182424180000ull)) * 25 +
      (COUNT_BITS(theKnights & 0x0000001818000000ull)) * 30;
  while (theKnights){
    rightmostBit = RIGHTMOST_BIT(theKnights);
    difference =
        rightmostBit < enemyKing ?
        (POSITION_BIT(enemyKing) - POSITION_BIT(rightmostBit)) :
        (POSITION_BIT(rightmostBit) - POSITION_BIT(enemyKing));
    totalBonus -= (difference % 8) + (difference / 8);
    theKnights ^= rightmostBit;
  }
  return totalBonus;
}

int BitBoardEvaluationHandlers::Bonuses::allBishopsPositionBonus(
    BitBoard& board,
    BitBoardPlayerColor myColor) {
  // Bishops get the following bonuses.
  int64 theBishops = board.view[BB_NEBUN + myColor * BB_PlayerGap];
  int64 enemyKing = board.view[BB_REGE + ((char)myColor ^ 1) * BB_PlayerGap];
  int64 rightmostBit;
  short int difference;
  int totalBonus =
      (COUNT_BITS(theBishops & 0xFF818181818181FFull)) * 14 +
      (COUNT_BITS(theBishops & 0x007E424242427E00ull)) * 16 +
      (COUNT_BITS(theBishops & 0x00003C24243C0000ull)) * 19 +
      (COUNT_BITS(theBishops & 0x0000001818000000ull)) * 22;
  while (theBishops) {
    rightmostBit = RIGHTMOST_BIT(theBishops);
    difference = (
        rightmostBit < enemyKing ?
        (POSITION_BIT(enemyKing) - POSITION_BIT(rightmostBit)) :
        (POSITION_BIT(rightmostBit) - POSITION_BIT(enemyKing)));
    totalBonus -= (((difference % 8) + (difference / 8)) & 0x00000003) * 5;
    theBishops ^= rightmostBit;
  }
  return totalBonus;
}

int BitBoardEvaluationHandlers::Bonuses::allRooksPositionBonus(
    BitBoard& board,
    BitBoardPlayerColor myColor){
  int64 theRooks = board.view[BB_TURN + myColor * BB_PlayerGap];
  int64 enemyKing = board.view[BB_REGE + ((char)myColor ^ 1) * BB_PlayerGap];
  int64 rightmostBit;
  short int difference;
  int totalBonus = 0;
  while (theRooks) {
    rightmostBit = RIGHTMOST_BIT(theRooks);
    difference = rightmostBit < enemyKing ?
        (POSITION_BIT(enemyKing) - POSITION_BIT(rightmostBit)) :
        (POSITION_BIT(rightmostBit) - POSITION_BIT(enemyKing));
    totalBonus -=
        (((((difference % 8) + (difference / 8)) - 1) & 0x00000001) + 1) * 5;
    theRooks ^= rightmostBit;
  }
  return totalBonus;
}

int BitBoardEvaluationHandlers::boardComparissonForWhite(const void* board1,
                                                         const void* board2){
  BitBoard* leftBoard = (BitBoard*) board1;
  BitBoard* rightBoard = (BitBoard*) board2;
  int whiteScore = 0, blackScore = 0, leftValue, rightValue;
  BitBoardEvaluationHandlers::fancyGNUEvaluation(
      *leftBoard, whiteScore, blackScore);
  leftValue = whiteScore - blackScore;
  BitBoardEvaluationHandlers::fancyGNUEvaluation(
      *rightBoard, whiteScore, blackScore);
  rightValue = whiteScore - blackScore;
  return -(leftValue - rightValue);
}

int BitBoardEvaluationHandlers::boardComparissonForBlack(const void* board1,
                                                         const void* board2) {
  BitBoard* leftBoard = (BitBoard*) board1;
  BitBoard* rightBoard = (BitBoard*) board2;
  int whiteScore = 0, blackScore = 0, leftValue, rightValue;
  BitBoardEvaluationHandlers::fancyGNUEvaluation(
      *leftBoard, whiteScore, blackScore);
  leftValue = blackScore - whiteScore;
  BitBoardEvaluationHandlers::fancyGNUEvaluation(
      *rightBoard, whiteScore, blackScore);
  rightValue = blackScore - whiteScore;
  return -(leftValue - rightValue);
}
